/*
 * This file is part of CAN4Home.utils.
 *
 * CAN4Home.utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CAN4Home.utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CAN4Home.utils.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 (c) William Chèvremont
 */

package org.can4home.utils;

import java.io.IOException;

/**
 * Abstract class for all CAN adaptators. Provide minimum functions: send packet and callback on receive.
 * @author William Chèvremont
 */
public abstract class CANAdaptator {

    /**
     * Interface for receive function.
     */
    public interface receiveCallback {
        /**
         * This fuction is called each time a packet is received.
         */
        public void onReceive(CANPacket pkt);
    }

    protected receiveCallback m_onReceive=null;
    
    /**
     * Set receive callback function
     * @param cbk Callback function.
     */
    public void setReceiveCallback(receiveCallback cbk) {
        m_onReceive = cbk;
    }
    
    /**
     * Send a CAN packet on the bus.
     * @param p Packet to send.
     */
    public abstract void send(CANPacket p) throws IOException;
}
