/*
 * This file is part of CAN4Home.utils.
 *
 * CAN4Home.utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CAN4Home.utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CAN4Home.utils.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 (c) William Chèvremont
 */

package org.can4home.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;

import org.lightpiio.GPIO;
import org.lightpiio.GPIORunCallback;
import org.lightpiio.SPI;


/**
 * This class is implementation for MCP2515 as CAN adaptator.
 * For best understanding, read MCP2515 datasheet.
 * @author William Chèvremont
 */
public class MCP2515 extends CANAdaptator {

    private SPI m_device;
    private Integer m_intPin = -1;
    protected GPIORunCallback m_threadCallback;

    private static final byte MCP2515_READRX = (byte) 0x90;
    private static final byte MCP2515_LOADTX = (byte) 0x40;
    private static final byte MCP2515_RTS = (byte) 0x80;
    private static final byte MCP2515_READREG = (byte) 0x03;
    private static final byte MCP2515_WRITEREG = (byte) 0x02;
    private static final byte MCP2515_RESET = (byte) 0xC0;
    private static final byte MCP2515_READST = (byte) 0xA0;
    private static final byte MCP2515_READRXST = (byte) 0xB0;
    private static final byte MCP2515_BITMODIFY = (byte) 0x05;
    
    public static final byte speed_500kbps = (byte) 0x00; /**< Value for for 500kbps CAN-speed */
    public static final byte speed_250kbps = (byte) 0x01; /**< Value for for 250kbps CAN-speed */
    public static final byte speed_125kbps = (byte) 0x03; /**< Value for for 125kbps CAN-speed */
    public static final byte speed_62p5kbps = (byte) 0x07; /**< Value for for 62.5kbps CAN-speed */
    
    public static final byte INTF_MERROR = (byte)0x80; /**< Interrupt flag for Message error */
    public static final byte INTF_WAKEUP = (byte) 0x40; /**< Interrupt flag on Wakeup interrupt */
    public static final byte INTF_ERROR = (byte) 0x20; /**< Interrupt flag on Error interrupt (multiple sources, see EFLG register)*/
    public static final byte INTF_TX2 = (byte) 0x10; /**< Interrupt flag on TX2 buffer empty */
    public static final byte INTF_TX1 = (byte) 0x08; /**< Interrupt flag on TX1 buffer empty */
    public static final byte INTF_TX0 = (byte) 0x04; /**< Interrupt flag on TX0 buffer empty */
    public static final byte INTF_RX1 = (byte) 0x02; /**< Interrupt flag on RX1 buffer full */
    public static final byte INTF_RX0 = (byte) 0x01; /**< Interrupt flag on RX0 buffer full */
    
    public static final byte AF_RXM0 = (byte) 0x20; /**< Acceptance filter mask 0 */
    public static final byte AF_RXM1 = (byte) 0x24; /**< Acceptance filter mask 1 */
    public static final byte AF_RXF0 = (byte) 0x00; /**< Acceptance filter filter 0*/
    public static final byte AF_RXF1 = (byte) 0x04; /**< Acceptance filter filter 1*/
    public static final byte AF_RXF2 = (byte) 0x08; /**< Acceptance filter filter 2*/
    public static final byte AF_RXF3 = (byte) 0x10; /**< Acceptance filter filter 3*/
    public static final byte AF_RXF4 = (byte) 0x14; /**< Acceptance filter filter 4*/
    public static final byte AF_RXF5 = (byte) 0x18; /**< Acceptance filter filter 5*/
    
    public static final byte MODE_CONFIG=(byte)0x80; /**< Configuration mode */
    public static final byte MODE_NORMAL=(byte) 0x00; /**< Normal mode */
    public static final byte MODE_SLEEP=(byte) 0x20; /**< Sleep mode */
    public static final byte MODE_LISTENONLY=(byte) 0x60; /**< Listen only mode */
    public static final byte MODE_LOOPBACK=(byte) 0x40; /**< Loopback mode */
    
    public static final byte BUFF_TX0 = (byte) 0x30; /**< TX0 buffer */
    public static final byte BUFF_TX1 = (byte) 0x40; /**< TX1 buffer */
    public static final byte BUFF_TX2 = (byte) 0x50; /**< TX2 buffer */
    public static final byte BUFF_RX0 = (byte) 0x60; /**< RX0 buffer */
    public static final byte BUFF_RX1 = (byte) 0x70; /**< RX1 buffer */
    
    class Locker {
        boolean m_N = true;
        public synchronized boolean acquire(long timeout) {
            m_N = false;
            try {
                wait(timeout);
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
            return m_N;
        }
        public synchronized void release() {
            m_N = true;
            notify();
        }
        public synchronized boolean isAvailable() {
            return m_N;
        }
    };
    
    protected Locker m_tx0=new Locker(), m_tx1 = new Locker(), m_tx2 = new Locker();
    protected ReentrantLock l_tx0 = new ReentrantLock(), l_tx1 = new ReentrantLock(), l_tx2 = new ReentrantLock();

    /**
     * Class constructor.
     * @param devname Linux file of spi communication (/dev/spidev1.0)
     * @param spispeed Speed of SPI communication (1000000 for 1MHz)
     * @param intPin Interrupt GPIO pin (-1 if not used)
     */
    public MCP2515(String devname, Integer spispeed, Integer intPin) throws IOException {
    
        System.out.println(Thread.currentThread().getContextClassLoader());
    
        m_device = SPI.getInstance(devname, spispeed, 0);
        
        if(intPin >= 0) {
            m_intPin = intPin;
            GPIO.export(m_intPin); // This can be long.
            GPIO.setDirection(m_intPin, true);
            GPIO.setupInterrupt(m_intPin, GPIO.INT_FALLING);

            m_threadCallback = GPIO.pinCallback(m_intPin, () -> { this.CanEventCallback(); }, 1000);
        }
    }
    
    /**
     * Send a CAN-packet on the bus. Manage to Rollover on transmit buffers. Return on message successfully sent.
     * @param p Packet to send
     * @throws IOException If unable to send packet (Multiple cause: bus error, not acknoledged on the bus, ...)
     */
    public void send(CANPacket p) throws IOException {
        send(p, 1000);
    }

    /**
     * Send a CAN-packet on the bus. Manage to Rollover on transmit buffers. Return on message successfully sent.
     * @param p Packet to send
     * @param timeout Timeout
     * @throws IOException If unable to send packet (Multiple cause: bus error, not acknoledged on the bus, ...)
     */
    public void send(CANPacket p, int timeout) throws IOException {
        while(true) {
            if(l_tx0.tryLock() && m_tx0.isAvailable()) {
                send(p, (byte)0);
                if(!m_tx0.acquire(timeout)) {
                    m_tx0.release();
                    l_tx0.unlock();
                    synchronized(this) {
                        notifyAll();
                    }
                    throw new IOException("Unable to send packet");
                }
                l_tx0.unlock();
                return;
            }
        
            if(l_tx1.tryLock() && m_tx1.isAvailable()) {
                send(p, (byte)1);
                if(!m_tx1.acquire(timeout)) {
                    m_tx1.release();
                    l_tx1.unlock();
                    synchronized(this) {
                        notifyAll();
                    }
                    throw new IOException("Unable to send packet");
                }
                l_tx1.unlock();
                return;
            }
            
            if(l_tx2.tryLock() && m_tx2.isAvailable()) {
                send(p, (byte)2);
                if(!m_tx2.acquire(timeout)) { // Timeout: release and notify blocked
                    m_tx2.release();
                    l_tx2.unlock();
                    synchronized(this) {
                        notifyAll();
                    }
                    throw new IOException("Unable to send packet");
                }
                l_tx2.unlock();
                return;
            }
            
            synchronized(this) {
                try {
                    wait();
                    Thread.sleep(30); // 30ms before next round
                } catch(InterruptedException e) {}
            }
        }
    }
    
    /**
     * Set CANbus speed
     * @param speed Mask of speed
     * @see speed_125kbps
     * @see speed_250kbps
     * @see speed_500kbps
     * @see speed_62p5kbps
     */
    public void setCanSpeed(byte speed) throws IOException {
        byte[] buf = new byte[3];
        buf[0] = (byte)0x01;
        buf[1] = (byte)0xD1;
        buf[2] = (byte)(speed & 0x3F);
        
        writeRegister((byte)0x28, buf);
    }
    
    /**
     * Set working mode. Return only when mode is changed.
     * @param mode Working mode.
     * @see MODE_CONFIG
     * @see MODE_LISTENONLY
     * @see MODE_LOOPBACK
     * @see MODE_NORMAL
     * @see MODE_SLEEP
     */
    public void setMode(byte mode) throws IOException {
        mode = (byte)(mode & 0xE0);
        bitModify((byte)0x0F, (byte)0xE0, mode);
        
        while((canStatus() & (byte)0xE0) != mode);
    }
    
    /**
     * Set one-shot mode. If enabled, try only once to send a packet.
     * @param en Enable one-shot mode.
     */
    public void setOneShot(boolean en) throws IOException {
        bitModify((byte)0x0F, (byte)0x08, (en) ? (byte)0x08 : (byte)0x00);
    }
    
    /**
     * Set abord flag. Abord all pending transmissions.
     * @param en Enable abord flag.
     */
    public void setAbordFlag(boolean en) throws IOException {
        bitModify((byte)0x0F,(byte)0x10, ((en)?(byte)0x10:(byte)0x00));
    }
    
    /**
     * Setup message control register. See datasheet for more informations.
     * @param msgbuf Id of message buffer.
     * @param conf Config. See datasheet.
     * @see BUFF_TX0
     * @see BUFF_TX1
     * @see BUFF_TX2
     * @see BUFF_RX0
     * @see BUFF_RX1
     */
    public void setMessageControlRegister(byte msgbuf, byte conf) throws IOException {
        writeRegister(msgbuf, conf);
    }
    
    /**
     * Setup message acceptance filters. See datasheet for more informations.
     * @param msgbuf Id of acceptance filter.
     * @param conf Config. See datasheet.
     * @see AF_RXF0
     * @see AF_RXF1
     * @see AF_RXF2
     * @see AF_RXF3
     * @see AF_RXF4
     * @see AF_RXF5
     * @see AF_RXM0
     * @see AF_RXM1
     */
    public void setupAcceptanceFilter(byte msgbuf, byte[] conf) throws IOException {
        if(conf.length != 4) throw new IOException("Config is 4-byte long!");
        
        writeRegister(msgbuf, conf);
    }
    
    /**
     * Setup message acceptance filter for specific node/group.
     * If groups and nodes are not null, they must have the same length.
     * @param bufno Buffer number (0,1)
     * @param groups List of allowed groups. If null, do not filter according to groups.
     * @param nodes List of allowed nodes. If null, do not filter according to nodes.
     */
    public void setupAcceptanceFilter(int bufno, byte groups[], byte nodes[]) throws IOException {
        if (bufno < 0 || bufno > 1) throw new IOException("Invalid buffer number!");
        if (bufno == 0 && ((groups != null && groups.length > 2) || (nodes != null && nodes.length > 2))) throw new IOException("Too much filters! (max is 2 for buffer 0)");
        if (bufno == 1 && ((groups != null && groups.length > 4) || (nodes != null && nodes.length > 4))) throw new IOException("Too much filters! (max is 4 for buffer 1)");
        if(nodes != null && groups != null && groups.length != nodes.length) throw new IOException("Groups and nodes must have the same length (or be null)");
        
        byte tmp[] = new byte[4];
        
        tmp[0] = (nodes == null) ? (byte)0x00 : (byte)0xFF;
        tmp[1] = (groups == null) ? (byte)0x00 : (byte)0xE3;
        tmp[2] = (byte)0;
        tmp[3] = (byte)0;
        
        setupAcceptanceFilter((byte)(AF_RXM0 + bufno*4), tmp);
        
        int max = (bufno == 0) ? 2 : 4;
        int N = (groups != null) ? groups.length : (nodes != null) ? nodes.length : 0;
        
        for(int i=0;i<N;i++) {
            tmp[0] = (nodes == null) ? (byte) 0 : nodes[i];
            tmp[1] = (byte)(0x08 | ((groups == null) ? 0 : (((groups[i] & 0x1C) << 3) | (groups[i] & 0x03) )));
            setupAcceptanceFilter((byte)(AF_RXF0 + (2*bufno + i)*4), tmp);
        }
        for(int i=N;i<max;i++) setupAcceptanceFilter((byte)(AF_RXF0 + (2*bufno + i)*4), tmp);
    }
    
    /**
     * Setup CAN config register. See datasheet for more informations.
     * @param conf Config (3bytes long)
     */
    public void setCanConfig(byte conf[]) throws IOException {
         if(conf.length != 3) throw new IOException("Config is 3-byte long!");
         
         writeRegister((byte)0x28, conf);
    }
    
    /**
     * Enable interrupts.
     * @param IF Interrupts to enable.
     * @see INTF_ERROR
     * @see INTF_MERROR
     * @see INTF_RX0
     * @see INTF_RX1
     * @see INTF_TX0
     * @see INTF_TX1
     * @see INTF_TX2
     * @see INTF_WAKEUP
     */
    public void setupInterrupts(byte IF) throws IOException {
        writeRegister((byte)0x2B, IF);
    }
    
    /**
     * Clear all pending interrupts.
     */
    public void clearInterrupts() throws IOException {
        writeRegister((byte)0x2C, (byte)0);
    }
    
    /**
     * Get current pending interrupts.
     * @return Register 0x2C (Interrupts flags registers)
     * @see INTF_ERROR
     * @see INTF_MERROR
     * @see INTF_RX0
     * @see INTF_RX1
     * @see INTF_TX0
     * @see INTF_TX1
     * @see INTF_TX2
     * @see INTF_WAKEUP
     */
    public byte getInterrupts() throws IOException {
        return readRegister((byte)0x2C);
    }
    
    /**
     * Get CAN status
     * @return Register 0x0E. See datasheet for more informations.
     */
    public byte canStatus() throws IOException {
        return readRegister((byte)0x0E);
    }
    
    /**
     * Reset MCP2515.
     */
    public void reset() throws IOException {
        m_device.transfer(MCP2515_RESET);
    }
    
    protected CANPacket readRxBuffer(int bufno) throws IOException {
        if (bufno < 0 || bufno > 1) {
            throw new IOException("Buffer number is out of range [0,1]");
        }

        byte[] tmp = new byte[14];

        tmp[0] = (byte) (MCP2515_READRX | bufno << 2); // Quick command for RX buffer read (starting from sidh)

        tmp = m_device.transfer(tmp);

        CANPacket r = new CANPacket();

        r.nodeAddress = tmp[1];
        r.groupAddress = (byte) ((tmp[2] & 0xE0) >> 3 | (tmp[2] & 0x3));
        r.command = tmp[3];
        r.cmdOpt = tmp[4];
        r.isRequest = (tmp[5] & 0x40) > 0;
        r.dataLength = (byte) (tmp[5] & 0x0F);

        if (r.dataLength > 8) {
            r.dataLength = 8;
        }

        if (!r.isRequest) {
            r.data = new byte[r.dataLength];
        }

        for (int i = 0; i < r.dataLength; i++) {
            r.data[i] = tmp[i + 6];
        }
        return r;
    }
    
    protected synchronized void CanEventCallback() {
        try {
            // logger.debug("GPIO: " + GPIO.digitalRead(m_intPin));

            while (!GPIO.digitalRead(m_intPin)) {
                byte st = readStatus();

                if ((st & 0x01) > 0) { // Buffer 0
                    CANPacket pkt = readRxBuffer(0); // This will clear interrupt
                    
                    if(m_onReceive != null) {
                        new Thread(() -> { this.m_onReceive.onReceive(pkt); }).start(); // Execute callback in another thread to prevent blocking
                    }
                }

                if ((st & 0x02) > 0) { // Buffer 1
                    CANPacket pkt = readRxBuffer(1); // This will clear interrupt

                    if(m_onReceive != null) {
                        new Thread(() -> { this.m_onReceive.onReceive(pkt); }).start(); // Execute callback in another thread to prevent blocking
                    }
                }

                if ((st & 0xA8) > 0) { // Tx0,1,2
                    if((st & 0x08)>0) { m_tx0.release(); bitModify((byte)0x2C, (byte)0x04, (byte)0); }
                    if((st & 0x20)>0) { m_tx1.release(); bitModify((byte)0x2C, (byte)0x08, (byte)0); }
                    if((st & 0x80)>0) { m_tx2.release(); bitModify((byte)0x2C, (byte)0x10, (byte)0); }
                    notifyAll();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void writeRegister(byte address, byte[] data) throws IOException {
        byte[] tmp = new byte[2 + data.length];

        tmp[0] = MCP2515_WRITEREG;
        tmp[1] = address;
        for (int i = 0; i < data.length; i++) {
            tmp[i + 2] = data[i];
        }

        m_device.transfer(tmp);
    }
    
    
    protected synchronized void send(CANPacket p, byte bufno) throws IOException {
        if (bufno < 0 || bufno > 2) {
            throw new IOException("Buffer out of range [0,1,2]");
        }

        // Build packet
        byte[] tmp = new byte[14];

        tmp[0] = (byte) (MCP2515_LOADTX | (bufno << 1));
        tmp[1] = p.nodeAddress;
        tmp[2] = (byte) (((p.groupAddress & 0x1C) << 3) | (p.groupAddress & 0x03) | 0x08);
        tmp[3] = p.command;
        tmp[4] = p.cmdOpt;

        if (p.dataLength > 8) {
            p.dataLength = 8;
        }

        if (p.dataLength < 0) {
            p.dataLength = 0;
        }

        tmp[5] = p.dataLength;
        if (p.isRequest) {
            tmp[5] |= 0x40;
        }

        // Fill data if not a RTR
        if (!p.isRequest) {
            for (int i = 0; i < p.dataLength; i++) {
                tmp[6 + i] = p.data[i];
            }
        }

        // Fill buffer
        m_device.transfer(tmp);

        // Delay RTS
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
        }

        // Request to Send
        m_device.transfer((byte) (MCP2515_RTS | (1 << bufno)));
    }

    protected void writeRegister(byte address, byte data) throws IOException {
        byte[] tmp = new byte[3];

        tmp[0] = MCP2515_WRITEREG;
        tmp[1] = address;
        tmp[2] = data;

        m_device.transfer(tmp);
    }

    protected byte[] readRegister(byte address, byte length) throws IOException {
        byte[] tmp = new byte[2 + length];
        tmp[0] = MCP2515_READREG;
        tmp[1] = address;

        tmp = m_device.transfer(tmp);

        byte[] ret = new byte[length];
        for (int i = 0; i < length; i++) {
            ret[i] = tmp[i + 2];
        }
        return ret;
    }

    protected byte readRegister(byte address) throws IOException {
        byte[] tmp = new byte[3];
        tmp[0] = MCP2515_READREG;
        tmp[1] = address;

        tmp = m_device.transfer(tmp);

        return tmp[2];
    }

    protected void bitModify(byte address, byte mask, byte value) throws IOException {
        byte[] tmp = new byte[4];
        tmp[0] = MCP2515_BITMODIFY;
        tmp[1] = address;
        tmp[2] = mask;
        tmp[3] = value;

        m_device.transfer(tmp);
    }

    protected byte readRxStatus() throws IOException {
        byte[] tmp = new byte[2];
        tmp[0] = MCP2515_READRXST;
        tmp = m_device.transfer(tmp);
        return tmp[1];
    }

    protected byte readStatus() throws IOException {
        byte[] tmp = new byte[2];
        tmp[0] = MCP2515_READST;
        tmp = m_device.transfer(tmp);
        return tmp[1];
    }
}
