/*
 * This file is part of CAN4Home.utils.
 *
 * CAN4Home.utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CAN4Home.utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CAN4Home.utils.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 (c) William Chèvremont
 */

package org.can4home.utils;

//TODO: write accessors to check ranges.

/**
 * Hold a CANPacket, within CAN4Home framemork.
 * @author William Chèvremont
 */
public class CANPacket {

    public static final byte CMD_DATA = (byte) 0x00; /**< DATA command identifier */
    public static final byte CMD_WRITE = (byte) 0x01; /**< WRITE DATA command identifier */
    public static final byte CMD_CHANINFO = (byte) 0x02; /**< CHANNEL INFORMATION command identifier */
    public static final byte CMD_SUBSCRIBE = (byte) 0x03; /**< SUBSRIBE TO CHANNEL command identifier */
    
    public static final byte CMD_DEBUG = (byte) 0x70; /**< DEBUG level information */
    public static final byte CMD_INFO = (byte) 0x71; /**< INFO level information */
    public static final byte CMD_WARN = (byte) 0x72; /**< WARN level information */
    public static final byte CMD_ERROR = (byte) 0x73; /**< ERROR level information */
    
    public static final byte CMD_REBOOT = (byte) 0x80; /**< Reboot node */
    public static final byte CMD_NODEBLD = (byte) 0x81; /**< Information that one node just enter bootloader mode */
    public static final byte CMD_ENTERBLD = (byte) 0x82; /**< Request the node to enter bootloader */
    public static final byte CMD_NODENORM = (byte) 0x83; /**< Information that one node just enter normal mode */
    
    public static final byte CMD_PINGPONG = (byte) 0x90; /**< PING (req) / PONG (data) protocol. */
    public static final byte CMD_SERIAL = (byte) 0x91; /**< Node serial number */
    public static final byte CMD_FIRMVER = (byte) 0x92; /**< Firmware version */
    public static final byte CMD_DEVSIG = (byte) 0x93; /**< Read device signature */
    public static final byte CMD_SETADDR = (byte) 0x94; /**< Set new address */
    
    public static final byte CMD_BLDREADPARAM = (byte) 0xB0; /**< Read bootloader parameters (only in bootloader mode)*/
    public static final byte CMD_BLDLOADFLASH = (byte) 0xB1; /**< Load a flash page  (only in bootloader mode)*/
    public static final byte CMD_BLDWRITEPAGE = (byte) 0xB2; /**< Write the loaded page  (only in bootloader mode)*/
    public static final byte CMD_BLDREADFLASH = (byte) 0xB3; /**< Read flash content  (only in bootloader mode)*/
    
    public byte groupAddress; /**< Group address. Valid range: [0-31] */
    public byte nodeAddress; /**< Node address. Valid range: [0-255] */
    public byte command; /**< Command identifier. See documentation. */
    public byte cmdOpt; /**< Command option. See documentation. */
    public boolean isRequest; /**< Is CAN request */
    public byte dataLength; /**< Data length. Valid range: [0-8] */
    public byte[] data = new byte[0]; /**< Data (only if not request). Maximum 8 bytes.*/

    /**
     * Convert to string format.
     * @return String that represent CAN packet.
     */
    @Override
    public String toString() {
        String s = String.format("%02x in %02x :: ", nodeAddress, groupAddress);
        s += String.format("%02x %02x", command, cmdOpt);
        if (isRequest) {
            s += " request " + Integer.toString(dataLength) + " bytes";
        } else {
            s += " data: ";
            for (int i = 0; i < data.length; i++) {
                s += String.format("%02x ", data[i]);
            }
        }
        return s;
    }
    
    /**
     * Convert to hash
     * @return hash
     */
    public Integer toHash() {
        Integer hash = (int) cmdOpt;
        hash += nodeAddress << 8;
        hash += groupAddress << 16;
        return hash;
    }
}
