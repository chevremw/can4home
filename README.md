# CAN4HomeAutomation

CAN4HomeAutomation is an application layer on CAN bus, dedicated for Home Automation. It
allow things to be interconnected in a reliable way, using the powerful CAN bus,
and define a standard for inter-things communications.

The protocol is definded as a strong typed variable exchange protocol. This means that
all data exchanged on the CAN bus will be typed in the Extended Arbitration field. The 
communication model is based on producer-consumer. Data exchange on the bus is
triggered either by external event or request.

You are free to extend this communication protocol for your own use. There are
user-costumizable fields for that. However:
- Use as most as possible standard defined feature
- Do not use standard defined feature for other purpose
