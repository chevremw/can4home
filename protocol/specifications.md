# Protocol specifications

Dated 2019-06-01
Version 0.1a

## Packet format

The packet format is based on extended CAN frame.
The packet header looks like:

| Node id | Group id | Command | Command option | 
| ------ | ------ | ------ | ------|
| 8 bits | 5 bits | 8 bits | 8 bits | 

RTR and data frame are used. Group + Node is the node address

Reserved address (All groups):
* Node=0x00: Reserved
* Node=0xFF: Broadcast in Group

Group=31: Reserved for specific purposes:
* Node without address (at first start) take a free address in this group
* Node=0xBB: Node in bootloader mode

## List of commands

This describe the availables commands and options.

Trigger description:
* RTR: an other node request a sending of this packet
* event: Event on the node holding the channel (Button push, timer, ...)

Byte order is bigEndian

### 0x00: Data
* Command option: Channel id
* Data: 1..8 bytes depending on channel type
* Trigger: RTR or event
* Channel 0 is reserved for the number of channels

### 0x01: Write data to channel
* Command option: Channel id
* Data: 1..8 bytes depending on channel type
* Trigger: event

### 0x02: Channel description
* Command option; Channel id
* Data: 1 byte
* Trigger: RTR
* type | 0x80 : Channel readable
* type | 0x40 : Channel writtable
* 0x00 : Non-existent channel
* 0x01 : uint8 (1 byte)
* 0x02 : uint16 (2 bytes)
* 0x03 : uint32 (4 bytes)
* 0x04 : uint64 (8 bytes)
* 0x05 : int8 (1 byte)
* 0x06 : int16 (2 bytes)
* 0x07 : int32 (4 bytes)
* 0x08 : int64 (8 bytes)
* 0x09 : Reserved for float32 IEEE754 (4 bytes)
* 0x0A : Reserved for float64 IEEE754 (8 bytes)
* 0x10 : Switch (1 byte)
  * 0: OFF
  * 1: ON
* 0x11 : Contact (1 byte)
  * 0x01 : PRESSED
  * 0x02 : RELEASED
  * 0x03 : RELEASED_LONG
* 0x12 : Color (4 bytes)
  * s : intensity (uint8)
  * R : Red (uint8)
  * G : Green (uint8)
  * B : Blue (uint8)
* 0x13 : Dimmer (1 byte)
  * Value 0-255 (uint8)
* 0x14 : Temperature (2 bytes)
  * uint16 DATA
  * T(°C) = (DATA - 27315)/100
* 0x15 : Relative humidity (1 byte)
  * RH(%) = DATA/2.55
* 0x16 : Roller (2 bytes)
  * Byte 1: state
    * 0x01 : top switch activated
    * 0x02 : Bottom switch activated
    * 0x04 : Is moving
    * 0x08 : Moving direction (up if set, don't care if 0x04 is unset)
    * 0x10 : Error
  * Byte 2: 0-255 opening value
* 0x20-0x3F: Free for users

### 0x03 : Activate event-based packet emission
* Command option: Channel id
* Data: 0 bytes
* Trigger: event
* If event-based packet are not acknoledged, event-based packet for this channel
is deactivated automatically

### 0x70 : Debug
* Command option: Debug code
* Data: 0 bytes
* Trigger: event
* Is sended in one-shot mode

### 0x71 : Information
* Command option: Information code
* Data: 0 bytes
* Trigger: event
* Is sended in one-shot mode

### 0x72 : Warning
* Command option: Warning code
* Data: 0 bytes
* Trigger: event
* Is sended in one-shot mode

### 0x73 : Error
* Command option: Error code
* Data: 0 bytes
* Trigger: event
* Is sended in one-shot mode

### 0x80 : Reboot node
* Command option: don't care
* Data: 0 bytes

### 0x81 : Node in bootloader mode
* Command option: UID0
* Data: UID[1..8]
* Trigger: When node is waiting for a "Go to bootloader" packet

### 0x82 : Enter bootloader
* Command option: UID0
* Data: UID[1..8]
* The uid specified is the one given in node in bootloader (0x81) packet.

### 0x83 : Node in normal mode
* Command option: don't care
* Data: 0 bytes
* Trigger: When node application code is running

### 0x90 : Ping/Pong
* Command option: Random number (PONG has to answer with same number)
* Data: 0 bytes
* PING is RTR frame
* PONG is Data frame

### 0x91 : Serial number
* Command option: Serial number byte 0
* Data: 8 bytes
* Trigger: RTR

### 0x92 : Firmware version
* Command option: Don't care
* Data: 3 bytes
* Trigger: RTR

### 0x93 : Read device signature
* Command option: Don't care
* Data: 3 bytes
* Trigger: RTR

### 0x94 : Set new address
* Command option: Don't care
* Data: 2 bytes
  * Byte 1: Group address
  * Byte 2: Node address
* Node first look if this address is free.
* Node reboot with new address if success.

### 0xB0 : Read bootloader parameters
* Only available in bootloader mode
* Command option: Don't care
* Data: 5 bytes
* Trigger: RTR
* Byte 1: Bootloader major version
* Byte 2: Bootloader minor version
* Byte 3: SIGNATURE 0
* Byte 4: SIGNATURE 1
* Byte 5: SIGNATURE 2

### 0xB1 : Load a flash page
* Only available in bootloader mode
* Command option: offset of first byte in the page
* Data: 8 bytes

### 0xB2 : Write the previously loaded page
* Only available in bootloader mode
* Command option: Don't care
* Data: 2 bytes: Page address
* Response with a RTR once the write operation is done.

### 0xB3 : Read flash content
* Only available in bootloader mode
* Command option; don't care
* Data: 2 bytes on request: flash address of first byte
* Data: 8 bytes on response: flash content
