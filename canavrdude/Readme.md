## CANAvrDude

Version 0.1a

CANAvrDude is a CAN network management tool.

Features:

- Rebooting node
- Reading device signature and uid
- Uploading firmware
- Reading firmware
- CAN network sniffing

Planned features:

- Node address assignment

### Building

```bash
 $ mkdir build
 $ cd build
 $ cmake ..
 $ make
```
