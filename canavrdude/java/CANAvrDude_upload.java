/*
 * This file is part of CAN4Home.canavrdude.
 *
 * CAN4Home.canavrdude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CAN4Home.canavrdude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CAN4Home.canavrdude.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 (c) William Chèvremont
 */
 
 
package org.can4home.canavrdude;

import org.can4home.utils.*;
import picocli.*;
import picocli.CommandLine.*;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;

@Command(name="upload", description="Upload a file to a node")
class CANAvrDude_upload implements Callable<Integer> {

    @ParentCommand
    private CANAvrDude parent;

    @Option(names= {"-a", "--address"}, description="Node address for reboot request. Default is node in bootloader address (1F:BB)", paramLabel="group:node")
    private String m_node="1F:BB";
    
    @Option(names= {"-s", "--start-address"}, description="Address from which to read. Will be ignored if not readonly.")
    private String m_startAddr = "";
    
    @Option(names= {"-n"}, description="Number of bytes to read. Will be ignored without if not readonly.")
    private String m_numberToRead = "";
    
    @Option(names= {"--uid"}, description="If set, ensure uid is correct. (useful when many nodes are stucked in booloader mode). Format: 01:23:45:67:89:AB:CD:EF:01 (hex)")
    private String m_uid=null;
    
    @ArgGroup(exclusive = true)
    MainOpt options = new MainOpt();
    
    static class MainOpt {
        @Option(names= {"-r", "--read"}, description="Read node content.")
        private boolean m_read=false;
        
        @Option(names= {"-w", "--write"}, description="Write only without verifying.")
        private boolean m_write=false;
    }
    
    @Parameters(paramLabel="FILE", arity = "1", description="File to upload")
    File m_file;
    
    private LinkedBlockingQueue<CANPacket> m_pkts = new LinkedBlockingQueue<CANPacket>();
    
    public Integer call() throws Exception {
    
        // check uid
        byte uid[] = new byte[9];
        
        if(m_uid != null) {
            String t[] = m_uid.split(":");
            if(t.length != 9) throw new IOException("Uid format mismatch");
            
            for(int i=0;i<9;i++) {
                if(! t[i].matches("^[0-9a-fA-F]{2}$")) throw new IOException("Uid format mismatch");
                
                uid[i] = (byte)Integer.parseInt(t[i], 16);
            }
        }    
    
        HexFile file = new HexFile(m_file);
        
        parent.init((byte)0x1f, (byte)0xbb);
        
        parent.m_adapt.setReceiveCallback((pkt) -> { // Filter only booloader-specific packets
            m_pkts.add(pkt);
        });
        
        // Reboot
        
        System.out.println("Rebooting node...");
        CANPacket pkt = new CANPacket();
        pkt.groupAddress = CANAvrDude.getGroup(m_node);
        pkt.nodeAddress = CANAvrDude.getNode(m_node);
        pkt.command = CANPacket.CMD_REBOOT;
        pkt.cmdOpt = 0x00;
        pkt.isRequest = false;
        pkt.dataLength = 0;
        
        parent.m_adapt.send(pkt);
        
        // Wait for node in bld packet
        outerloop:
        for(;;) {
            pkt = m_pkts.take();
            System.out.print(".");
            
             if(pkt.command == CANPacket.CMD_NODEBLD) { // If provided, target specific uid
                 
                 if(m_uid != null) {
                    if(uid[0] != pkt.cmdOpt) continue;
                    if(pkt.data.length != 8) continue;
                    //System.out.println(String.format("%02x %02x %b",uid[0], pkt.cmdOpt, uid[0] == pkt.cmdOpt));
                    for(int i=0;i<pkt.data.length;i++) {
                        //System.out.println(String.format("%02x %02x %b",uid[i+1], pkt.data[i], uid[i+1] == pkt.data[i]));
                        if(uid[i+1] != pkt.data[i]) continue outerloop;
                    }
                 }
                 
                 break;
             }
        }
        
        System.out.println("Node in bootloader state.\n Sending enter bootloader packet.");
        
        pkt.groupAddress = (byte)0x1F; // Should be useless
        pkt.nodeAddress = (byte)0xBB;
        pkt.command = CANPacket.CMD_ENTERBLD;
        // Note: keep uid from previous
        
        // Send enter to bootloader
        parent.m_adapt.send(pkt);
        Thread.currentThread().sleep(500); // Need to delay: Wait to be into loop
        
        // Check device signature
        pkt.command = CANPacket.CMD_BLDREADPARAM;
        pkt.isRequest = true;
        pkt.data = new byte[0];
        pkt.dataLength = 0;
        parent.m_adapt.send(pkt);
        
        do {
            pkt = m_pkts.take();
        } while(pkt.command != CANPacket.CMD_BLDREADPARAM);
        
        if(pkt.isRequest || pkt.data.length != 5) throw new IOException("Device signature fail!");
        
        int bootloader_maj = pkt.data[0];
        int bootloader_min = pkt.data[1];
        
        byte devsig[] = new byte[3];
        devsig[0] = pkt.data[2];
        devsig[1] = pkt.data[3];
        devsig[2] = pkt.data[4];
        
        System.out.println(String.format("Bootloader version: %d.%d :: Device: %02x:%02x:%02x", 
                bootloader_maj, bootloader_min,
                devsig[0], devsig[1], devsig[2]));
        
        CANPacket r_pkt;
        // Start loading, write and verify
        if(!options.m_read) {

            // TODO: 
            int pagesize = 64; // in word!
            pagesize *= 2; // in bytes;
            
            file.load(pagesize);
            short addr = file.startAddr();
            
            while (addr < file.startAddr() + file.datas().length) { // Write is done page-per-page
                pkt.command = CANPacket.CMD_BLDLOADFLASH;
                pkt.dataLength = 8;
                pkt.isRequest = false;
                pkt.data = new byte[8];
                do { // Load page
                    pkt.cmdOpt = (byte)(addr & (pagesize-1));
                    for(int i=0;i<8;i++) pkt.data[i] = file.datas()[addr+i];
                    System.out.print(".");
                    parent.m_adapt.send(pkt);
                    
                    do {
                      r_pkt = m_pkts.take();
                    } while(r_pkt.command != CANPacket.CMD_BLDLOADFLASH);
                    
                    addr += 8;
                } while((addr % pagesize) != 0);
                
                // Write page
                
                pkt.command = CANPacket.CMD_BLDWRITEPAGE;
                pkt.dataLength = 2;
                pkt.data = new byte[2];
                pkt.data[1] = (byte)(((addr - pagesize) & 0x00FF00) >> 8); 
                pkt.data[0] = (byte)((addr - pagesize) & 0x0000FF);
                
                System.out.println(String.format("Write page 0x%04x",addr-pagesize));
                
                parent.m_adapt.send(pkt);
                do {
                    r_pkt = m_pkts.take();
                } while(r_pkt.command != CANPacket.CMD_BLDWRITEPAGE);
            }           
            
            if(!options.m_write) { // Verify if not write only.
                pkt.command = CANPacket.CMD_BLDREADFLASH;
                pkt.cmdOpt = 0x00;
                pkt.isRequest = false;
                pkt.dataLength = 2;
                pkt.data = new byte[2];
                
                byte tmp[] = new byte[file.datas().length];
            
                System.out.println("Verifying.");
                for(int i=file.startAddr();i < file.startAddr() + file.datas().length;i+=8) {
                    
                    pkt.data[0] = (byte) (i & 0x000000FF);
                    pkt.data[1] = (byte)((i & 0x0000FF00) >> 8);
                
                    parent.m_adapt.send(pkt);
                    System.out.print(".");
                    
                    do {
                        r_pkt = m_pkts.take();
                    } while(r_pkt.command != CANPacket.CMD_BLDREADFLASH);
                    
                    for(int j=0;j<8;j++) tmp[i + j - file.startAddr()] = r_pkt.data[j];
                }
                
                HexFile file_ = new HexFile(new File(m_file+".verify"));
                file_.load(file.startAddr(), tmp);
                
                if(file_.equals(file)) {
                    System.out.println("Verify success!");
                } else {
                    file_.write();
                    System.err.println("Error while verifying. Read results stored to "+m_file+".verify");
                    return -1;
                }
            }
        
        } else { // Read only
        
            pkt.command = CANPacket.CMD_BLDREADFLASH;
            pkt.cmdOpt = 0x00;
            pkt.isRequest = false;
            pkt.dataLength = 2;
            pkt.data = new byte[2];
        
            int N = 0;
            int startaddr = 0;
            
            if(m_numberToRead.startsWith("0x")) {
                N = Integer.parseInt("0"+m_numberToRead.substring(2), 16);
            } else {
                N = Integer.parseInt(m_numberToRead);
            }
            
            if(m_startAddr.startsWith("0x")) {
                startaddr = Integer.parseInt("0"+m_startAddr.substring(2), 16);
            } else {
                startaddr = Integer.parseInt(m_startAddr);
            }
            
            if(N <= 0) throw new IOException("No bytes to read!");
            
            while(N % 8 != 0) N++;
            
            byte tmp[] = new byte[N];
            
            System.out.println("Only read memory.");
            for(int i=startaddr;i < startaddr + N;i+=8) {
                
                pkt.data[0] = (byte) (i & 0x000000FF);
                pkt.data[1] = (byte)((i & 0x0000FF00) >> 8);
            
                parent.m_adapt.send(pkt);
                System.out.print(".");
                
                do {
                    r_pkt = m_pkts.take();
                } while(r_pkt.command != CANPacket.CMD_BLDREADFLASH);
                
                for(int j=0;j<8;j++) tmp[i + j - startaddr] = r_pkt.data[j];
            }
            
            file.load((short)startaddr, tmp);
            file.write();
            System.out.println("\nFinished!");
        }
        return 0;
    }
}
 
