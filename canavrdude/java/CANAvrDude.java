/*
 * This file is part of CAN4Home.canavrdude.
 *
 * CAN4Home.canavrdude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CAN4Home.canavrdude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CAN4Home.canavrdude.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 (c) William Chèvremont
 */



package org.can4home.canavrdude;

import org.can4home.utils.*;

import picocli.*;
import picocli.CommandLine.*;

import java.io.IOException;
import java.util.regex.*;
import java.util.concurrent.Callable;

@Command(name="CANAvrDude",
        description="Manage AVRs in a CAN network",
        version="CANAvrDude 1.0a",
        subcommands = {
            CANAvrDude_sniffer.class,
            CANAvrDude_upload.class,
            CANAvrDude_reboot.class,
            CANAvrDude_network.class
        })
public class CANAvrDude implements Callable<Integer>{

    @Option(names = { "-h", "--help" }, usageHelp = true, description = "Display a help message")
    private boolean m_helpRequested = false;
    
    @Option(names = {"-d", "--device" }, description = "MCP2515 device (default=/dev/spidev1.0)")
    private String m_device = "/dev/spidev1.0";
    
    @Option(names = {"-b", "--baudrate"}, description = "SPI communication speed, default=1000000")
    private Integer m_spispeed = 1000000;
    
    @Option(names = {"-i", "--intpin"}, description = "Interrupt pin number (default=10)")
    private Integer m_intpin = 10;
    
    MCP2515 m_adapt = null;

    public static void main(String[] args) {
        int exitCode = new CommandLine(new CANAvrDude()).execute(args);
        System.exit(exitCode);
    }
    
    void init(byte group, byte node) throws IOException {
        m_adapt = new MCP2515(m_device, m_spispeed, m_intpin);
        
        m_adapt.reset();
        m_adapt.setMode(MCP2515.MODE_CONFIG);
        
        m_adapt.setCanSpeed(MCP2515.speed_125kbps);
        m_adapt.setupInterrupts((byte)(MCP2515.INTF_RX0 | MCP2515.INTF_RX1 | MCP2515.INTF_TX0 | MCP2515.INTF_TX1 | MCP2515.INTF_TX2));
        m_adapt.setMessageControlRegister(MCP2515.BUFF_RX0, (byte)0x04); // Turn filters on, rollover 0-1
        m_adapt.setMessageControlRegister(MCP2515.BUFF_RX1, (byte)0x00); // Turn filters on
        
        byte groups[] = new byte[1];
        groups[0] = group;
        
        byte nodes[] = new byte[1];
        nodes[0] = node;
        
        m_adapt.setupAcceptanceFilter(0, groups, nodes);
        m_adapt.setupAcceptanceFilter(1, groups, nodes);
        
        m_adapt.setMode(MCP2515.MODE_NORMAL);
    }
    
    void init(byte group) throws IOException {
        m_adapt = new MCP2515(m_device, m_spispeed, m_intpin);
        
        m_adapt.reset();
        m_adapt.setMode(MCP2515.MODE_CONFIG);
        
        m_adapt.setCanSpeed(MCP2515.speed_125kbps);
        m_adapt.setupInterrupts((byte)(MCP2515.INTF_RX0 | MCP2515.INTF_RX1 | MCP2515.INTF_TX0 | MCP2515.INTF_TX1 | MCP2515.INTF_TX2));
        m_adapt.setMessageControlRegister(MCP2515.BUFF_RX0, (byte)0x04); // Turn filters on, rollover 0-1
        m_adapt.setMessageControlRegister(MCP2515.BUFF_RX1, (byte)0x00); // Turn filters on
        
        byte groups[] = new byte[1];
        groups[0] = group;
        m_adapt.setupAcceptanceFilter(0, groups, null);
        m_adapt.setupAcceptanceFilter(1, groups, null);
        
        m_adapt.setMode(MCP2515.MODE_NORMAL);
    }
    
    void init() throws IOException {
        m_adapt = new MCP2515(m_device, m_spispeed, m_intpin);
        
        m_adapt.reset();
        m_adapt.setMode(MCP2515.MODE_CONFIG);
        
        m_adapt.setCanSpeed(MCP2515.speed_125kbps);
        m_adapt.setupInterrupts((byte)(MCP2515.INTF_RX0 | MCP2515.INTF_RX1 | MCP2515.INTF_TX0 | MCP2515.INTF_TX1 | MCP2515.INTF_TX2));
        m_adapt.setMessageControlRegister(MCP2515.BUFF_RX0, (byte)0x64); // Turn filters off, rollover 0-1
        
        m_adapt.setMode(MCP2515.MODE_NORMAL);
    }
    
    static void checkAddr(String addr) throws IOException {
        Pattern reg = Pattern.compile("([01][0-9A-F]):([0-9A-F]{2})");
        Matcher match = reg.matcher(addr.toUpperCase());
        
        if(!match.matches()) throw new IOException("Invalid CAN address!");
    }
    
    static byte getGroup(String addr) throws IOException {
        checkAddr(addr);
        Pattern reg = Pattern.compile("([01][0-9A-F]):([0-9A-F]{2})");
        Matcher match = reg.matcher(addr.toUpperCase());
        match.matches();
        return (byte)Integer.parseInt(match.group(1),16);
    }
    
    static byte getNode(String addr) throws IOException {
        checkAddr(addr);
        Pattern reg = Pattern.compile("([01][0-9A-F]):([0-9A-F]{2})");
        Matcher match = reg.matcher(addr.toUpperCase());
        match.matches();
        return (byte)Integer.parseInt(match.group(2),16);
    }
    
    @Override
    public Integer call() throws Exception { // your business logic goes here...
        System.err.println("Please specify subcommand.");
        return 0;
    }
}


