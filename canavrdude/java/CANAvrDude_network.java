/*
 * This file is part of CAN4Home.canavrdude.
 *
 * CAN4Home.canavrdude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CAN4Home.canavrdude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CAN4Home.canavrdude.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 (c) William Chèvremont
 */
 
 
package org.can4home.canavrdude;

import org.can4home.utils.*;

import picocli.*;
import picocli.CommandLine.*;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.LinkedBlockingQueue;

@Command(name="network", description="CAN network management. Scan CAN network")
class CANAvrDude_network implements Callable<Integer> {

    @ParentCommand
    private CANAvrDude parent;
    
    @Parameters(description="Group to scan. accept hex (starting with 0x) or decimal", arity="1", paramLabel="group")
    private String m_group=null;
    
    private LinkedBlockingQueue<CANPacket> m_pkts = new LinkedBlockingQueue<CANPacket>();
    
    public Integer call() throws Exception {
        
        int group = -1;
        
        if(m_group != null) {
            if(m_group.startsWith("0x"))
                group = (byte)Integer.parseInt(m_group.substring(2), 16);
            else
                group = (byte)Integer.parseInt(m_group);
                
            if(group < 0 || group > 31) throw new IOException("Invalid group!");
        }
    
        CANPacket pkt = new CANPacket();
        pkt.groupAddress = (byte)group;
        pkt.cmdOpt = 0x00;
        pkt.isRequest = true;
    
        parent.init((byte)group);
        
        // Setup receiving and send request
        parent.m_adapt.setReceiveCallback((pkt_) -> {
            m_pkts.add(pkt_);
        });

        System.out.println("GG:NN [ devsig ] uid\n-------------------------------------------");
        
        for(int j=1;j<254;j++) {
            pkt.nodeAddress  = (byte)j;
            
            pkt.command = CANPacket.CMD_DEVSIG;
            parent.m_adapt.send(pkt);
            
            CANPacket pkt_r = null;
            do {
                pkt_r = m_pkts.poll(50,  TimeUnit.MILLISECONDS);
            } while(pkt_r != null && pkt_r.command != CANPacket.CMD_DEVSIG && pkt.nodeAddress != (byte)j);
            
            if(pkt_r != null) {
                pkt.command = CANPacket.CMD_SERIAL;
                parent.m_adapt.send(pkt);
                
                CANPacket pkt_r2=null;
                do {
                    pkt_r2 = m_pkts.poll(50,  TimeUnit.MILLISECONDS);
                } while(pkt_r2 != null && pkt_r2.command != CANPacket.CMD_SERIAL && pkt_r2.nodeAddress != (byte)j);
                
                if(pkt_r2 != null)
                    System.out.println(String.format("%02x:%02x [%02x:%02x:%02x] %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x",
                                                    group, j,
                                                    pkt_r.data[0],
                                                    pkt_r.data[1],
                                                    pkt_r.data[2],
                                                    pkt_r2.cmdOpt,
                                                    pkt_r2.data[0],
                                                    pkt_r2.data[1],
                                                    pkt_r2.data[2],
                                                    pkt_r2.data[3],
                                                    pkt_r2.data[4],
                                                    pkt_r2.data[5],
                                                    pkt_r2.data[6],
                                                    pkt_r2.data[7]));
            }
        }

        return 0;
    }
}
 

