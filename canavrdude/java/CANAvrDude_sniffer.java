/*
 * This file is part of CAN4Home.canavrdude.
 *
 * CAN4Home.canavrdude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CAN4Home.canavrdude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CAN4Home.canavrdude.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 (c) William Chèvremont
 */
 
 
package org.can4home.canavrdude;

import org.can4home.utils.*;

import picocli.*;
import picocli.CommandLine.*;
import java.io.IOException;
import java.util.concurrent.Callable;


@Command(name="sniffer", description="Monitor traffic on CAN bus")
class CANAvrDude_sniffer implements Callable<Integer> {

    @ParentCommand
    private CANAvrDude parent;
    
    @ArgGroup(exclusive = true)
    MainOpt options = new MainOpt();
    
    static class MainOpt {
        @Option(names= {"-n", "--node"}, description="Filter from specific node address (hex)", paramLabel="group:node")
        private String m_nodeFilter;
        
        @Option(names= {"-g", "--group"}, description="Filter from specific group (hex)",paramLabel="group")
        private String m_groupFilter;
    }
    
    public Integer call() throws Exception {
        
        if(options.m_nodeFilter == null && options.m_groupFilter == null) { // no filter
            parent.init();
        } else if(options.m_nodeFilter != null) { // node filter
            parent.init(CANAvrDude.getGroup(options.m_nodeFilter), CANAvrDude.getNode(options.m_nodeFilter));
        } else { // group filter
            byte grp = (byte) Integer.parseInt(options.m_groupFilter);
            
            if(grp < 0 || grp > 31) throw new IOException("Invalid group!");
        
            parent.init(grp);
        }
        
        parent.m_adapt.setReceiveCallback((pkt) -> {
            System.out.println(pkt);
        });
        
        synchronized(this) {
            wait();
        }
        
        return 0;
    }    
}
