/*
 * This file is part of CAN4Home.canavrdude.
 *
 * CAN4Home.canavrdude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CAN4Home.canavrdude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CAN4Home.canavrdude.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 (c) William Chèvremont
 */
 
 
package org.can4home.canavrdude;

import java.io.*;
import java.util.regex.*;
import java.util.Vector;


/**
 * This class hold a hex-file.
 * @author William Chèvremont
 */
public class HexFile {

    private File m_file;
    private short m_startAddr;
    private byte m_bytes[];
    
    private class HexLine {
        static final byte TYPE_DATA= (byte)0x00;
        static final byte TYPE_EOF = (byte)0x01;
        static final byte TYPE_ESA = (byte)0x02;
        static final byte TYPE_SSA = (byte)0x03;
        static final byte TYPE_ELA = (byte)0x04;
        static final byte TYPE_SLA = (byte)0x05;

        byte type;
        short address;
        byte datas[];
        Pattern regex = Pattern.compile("^:([0-9A-F]{2})([0-9A-F]{4})(0[0-5])((?:[0-9A-F]{2})*)([0-9A-F]{2})$");
        
        HexLine() {
        
        }
        
        void process(String line) throws IOException {
            Matcher match = regex.matcher(line.toUpperCase());
            
            if(!match.matches()) throw new IOException("Wrong line format!");
            
            Integer size = Integer.parseInt("0"+match.group(1),16);
            address = (short)Integer.parseInt("0"+match.group(2),16);
            type = (byte)Integer.parseInt("0"+match.group(3), 16);
            Integer checksum = Integer.parseInt("0"+match.group(5), 16);
            
            if(size != match.group(4).length()/2) throw new IOException("Size mismatch");
            
            datas = new byte[size];
            
            for(int i=0;i<datas.length; i++) {
                datas[i] = (byte)Integer.parseInt(match.group(4).substring(2*i, 2*i+2), 16);
            }
            
            if((checksum() & 0x00FF) - checksum != 0) throw new IOException("Checksum fail!");
        }
        
        byte checksum() {
            Integer chk = datas.length + type + (address & 0x00FF) + ((address & 0x00FF00) >> 8);
            
            for(int i=0;i<datas.length; i++) {
                chk += datas[i];
            }
            
            return (byte)((0x100 - (chk & 0x00FF)) & 0x00FF);
        }
        
        @Override
        public String toString() {
            String ret = ":"+String.format("%02X%04X%02X", datas.length, address, type);
            for(int i=0;i<datas.length;i++) {
                ret += String.format("%02X", datas[i]);
            }
            return ret + String.format("%02X", checksum());
        }
    }
    
    /**
     * Class contructor.
     * @param file File to load/write
     */
    public HexFile(File file) {
        m_file = file;
    }
    
    /**
     * Load from file.
     * @param pagesize Page size (in byte). Datas will be page-aligned
     * @throws IOException if error in the file.
     */
    public void load(int pagesize) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(m_file)); 
        
        int startAddr = 0;
        int stopAddr = 0;
        Boolean first = true;
        
        String st;
        Vector<HexLine> hl = new Vector<HexLine>();
        while ((st = br.readLine()) != null) {
            HexLine ll = new HexLine();
            ll.process(st);
            
            if(ll.type == HexLine.TYPE_DATA) {
                if(first) {
                    startAddr = ll.address;
                    stopAddr = ll.address + ll.datas.length;
                    first = false;
                }
                
                if(startAddr > ll.address) startAddr = ll.address;
                if(stopAddr < ll.address + ll.datas.length) stopAddr = ll.address + ll.datas.length;
            }
            
            hl.add(ll);
        }
        
        // Align to pages
        while((startAddr % pagesize) != 0) startAddr--;
        while((stopAddr % pagesize) != 0) stopAddr++;
        
        m_bytes = new byte[stopAddr - startAddr];
        m_startAddr = (short)startAddr;
        
        // Default is 0xff (nop)
        for(int i=0;i<m_bytes.length;i++) m_bytes[i] = (byte)0xFF;
        
        for(int i=0;i<hl.size();i++) {
            for(int j=0;j<hl.get(i).datas.length;j++) {
                if(hl.get(i).type == HexLine.TYPE_DATA)
                    m_bytes[hl.get(i).address - m_startAddr + j] = hl.get(i).datas[j];
            }
        }
    }
    
    /**
     * Get start address.
     */
    public short startAddr() {
        return m_startAddr;
    }
    
    /**
     * Get datas.
     */
    public byte[] datas() {
        return m_bytes;
    }
    
    /**
     * Load data
     * @param addr First address
     * @param data Data bytes
     */
    public void load(short addr, byte data[]) {
        m_startAddr = addr;
        m_bytes = data;
    }    
    
    /**
     * Write data to file
     * @throws IOException on error.
     */
    public void write() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(m_file));
        int bid = 0;
        HexLine ll = new HexLine();
        ll.type = 0;
        while(bid < m_bytes.length) {
            ll.address = (short)(m_startAddr + bid);
            int len = (m_bytes.length - bid > 0x10) ? 0x10 : m_bytes.length - bid;
            ll.datas = new byte[len];
            for(int i=0;i<len;i++) ll.datas[i] = m_bytes[bid+i];
            bid += len;
            writer.write(ll.toString()+"\n");
        }
        ll.type = HexLine.TYPE_ESA;
        ll.address = 0;
        ll.datas = new byte[4];
        ll.datas[0] = 0;
        ll.datas[1] = 0;
        ll.datas[2] = (byte)((m_startAddr & 0x00FF00) >> 8);
        ll.datas[3] = (byte)(m_startAddr & 0x0000FF);
        writer.write(ll.toString()+"\n");
        ll.type = HexLine.TYPE_EOF;
        ll.datas = new byte[0];
        writer.write(ll.toString()+"\n");
        writer.close();
    }
    
    public boolean equals(final HexFile f) {
        if(m_startAddr != f.m_startAddr) { return false; }
        if(m_bytes.length != f.m_bytes.length) { return false; }
        
        for(int i=0;i<m_bytes.length;i++) {
            if(m_bytes[i] != f.m_bytes[i]) { return false; }
        }
        
        return true;
    }
}
