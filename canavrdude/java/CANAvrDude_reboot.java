/*
 * This file is part of CAN4Home.canavrdude.
 *
 * CAN4Home.canavrdude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CAN4Home.canavrdude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CAN4Home.canavrdude.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 (c) William Chèvremont
 */
 
 
package org.can4home.canavrdude;

import org.can4home.utils.*;

import picocli.*;
import picocli.CommandLine.*;

import java.io.File;
import java.util.concurrent.Callable;

@Command(name="reboot", description="Reboot a node")
class CANAvrDude_reboot implements Callable<Integer> {

    @ParentCommand
    private CANAvrDude parent;

    @Parameters(description="Node address.", arity="1", paramLabel="group:node")
    private String m_node=null;
    
    public Integer call() throws Exception {
        CANPacket pkt = new CANPacket();
        pkt.groupAddress = CANAvrDude.getGroup(m_node);
        pkt.nodeAddress = CANAvrDude.getNode(m_node);
        pkt.command = CANPacket.CMD_REBOOT;
        pkt.cmdOpt = 0x00;
        pkt.isRequest = false;
        pkt.dataLength = 0;
    
        System.out.println(pkt);
    
        parent.init();
        parent.m_adapt.send(pkt);
    
        System.out.println("Reboot packet send!");
        return 0;
    }    
}
 
