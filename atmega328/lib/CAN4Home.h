
#pragma once

#include <avr/boot.h>
#include "MCP2515.h"

#define CMD_DATA		0x00
#define CMD_WRITEDATA	0x01
#define CMD_CHANDESC	0x02
#define CMD_CHANEVENT	0x03

#define CMD_DEBUG		0x70
#define CMD_INFO		0x71
#define CMD_WARNING		0x72
#define CMD_ERROR		0x73

#define CMD_REBOOT		0x80
#define CMD_NODEBLD		0x81
#define CMD_ENTERBLD	0x82
#define CMD_NODENORMAL	0x83

#define CMD_PINGPONG	0x90
#define CMD_SERIAL		0x91
#define CMD_FIRMVER		0x92
#define CMD_DEVSIGN		0x93
#define CMD_SETADDR		0x94

#define BLD_PARAMS		0xB0
#define BLD_LOADFLASH	0xB1
#define BLD_WRITEFLASH	0xB2
#define BLD_READFLASH	0xB3


class CAN4Home
{
private:
	struct s_Channel {
		uint8_t type;
		uint8_t numBytes;
		bool event;
		void (*onRequest)(uint8_t*, uint8_t, uint8_t);
		void (*onWrite)(uint8_t*, uint8_t, uint8_t);
		struct s_Channel *next;
	};
	typedef struct s_Channel Channel;
	
public:
	
	struct s_CANPacket {
		uint8_t nodeId;
		uint8_t groupId;
		uint8_t command, cmdopt;
		uint8_t N;
		uint8_t data[8];
		bool isRequest;
	};
	typedef struct s_CANPacket CANPacket;
	
	typedef enum {
		READ 	= 0x80,
		WRITE 	= 0x40,
		
		UINT8	= 0x01,
		UINT16 	= 0x02,
		UINT32  = 0x03,
		UINT64  = 0x04,
		INT8 	= 0x05,
		INT16 	= 0x06,
		INT32 	= 0x07,
		INT64 	= 0x08,
		FLOAT32 = 0x09,
		FLOAT64 = 0x0A,
		
		SWITCH	= 0x10,
		CONTACT = 0x11,
		COLOR 	= 0x12,
		DIMMER 	= 0x13,
		TEMP	= 0x14,
		RELHUM 	= 0x15,
		ROLLER 	= 0x16		
	} ChanType;
	
	CAN4Home(uint8_t address, uint8_t group, uint8_t CS_pin, bool snif=false);
	
	uint8_t addChannel(ChanType type, uint8_t N, void (*onRequest)(uint8_t*, uint8_t, uint8_t), void (*onWrite)(uint8_t*, uint8_t, uint8_t)=0);
	CANPacket const& readNextPacket();
    bool sendPacket(CANPacket const& p);
	void writeData(uint8_t chanid, uint8_t* data, uint8_t count, bool event = true, bool onlyOnce=false);
	void sendChanDesc(uint8_t chanid, Channel *c);
    void sendSerial();
    void sendSignature();
	
private:
	
	uint8_t addChannel(Channel* chan);
	
	uint8_t m_nodeAddress, m_groupAddress;
	MCP2515 m_controller;
	Channel *m_chans;
	CANPacket m_lastPacket;
	uint8_t m_chanCount;
	bool m_sniffer;
};

/* Watchdog settings */
#define WATCHDOG_OFF    (0)
#define WATCHDOG_16MS   (_BV(WDE))
#define WATCHDOG_32MS   (_BV(WDP0) | _BV(WDE))
#define WATCHDOG_64MS   (_BV(WDP1) | _BV(WDE))
#define WATCHDOG_125MS  (_BV(WDP1) | _BV(WDP0) | _BV(WDE))
#define WATCHDOG_250MS  (_BV(WDP2) | _BV(WDE))
#define WATCHDOG_500MS  (_BV(WDP2) | _BV(WDP0) | _BV(WDE))
#define WATCHDOG_1S     (_BV(WDP2) | _BV(WDP1) | _BV(WDE))
#define WATCHDOG_2S     (_BV(WDP2) | _BV(WDP1) | _BV(WDP0) | _BV(WDE))
#ifndef __AVR_ATmega8__
#define WATCHDOG_4S     (_BV(WDP3) | _BV(WDE))
#define WATCHDOG_8S     (_BV(WDP3) | _BV(WDP0) | _BV(WDE))
#endif


