
#include "MCP2515.h"

MCP2515::MCP2515(uint8_t cs) : 
	m_CSpin(cs)
{
	pinMode(m_CSpin, OUTPUT);
	digitalWrite(m_CSpin, HIGH);
	SPI.begin();
	SPI.setBitOrder(MSBFIRST);
	SPI.setClockDivider(SPI_CLOCK_DIV16);
	SPI.setDataMode(SPI_MODE0);
}

void MCP2515::reset()
{
	digitalWrite(m_CSpin, LOW);
	SPI.transfer(0xC0);
	digitalWrite(m_CSpin, HIGH);
}

bool MCP2515::readRxBuffer(uint8_t bufno, uint8_t* buf, uint8_t nRead)
{
	if(bufno > 1)
		return false;
	
	if(nRead > 13)
		return false;
	
	digitalWrite(m_CSpin, LOW);
	SPI.transfer(0x90 | (bufno << 2));
	for(uint8_t i(0);i<nRead;i++) buf[i] = SPI.transfer(0x00);
	digitalWrite(m_CSpin, HIGH);
	
	return true;
}

bool MCP2515::send(uint8_t bufno, uint8_t* buf, uint8_t nWrite)
{
	if(bufno > 2)
		return false;
	
	if(nWrite > 13)
		return false;
	
	// Fill buffer
	digitalWrite(m_CSpin, LOW);
	SPI.transfer(0x40 | (bufno << 1));
	for(uint8_t i(0);i<nWrite;i++) SPI.transfer(buf[i]);
	digitalWrite(m_CSpin, HIGH);
	
	delay(1);
	
	// Request to Send
	digitalWrite(m_CSpin, LOW);
	SPI.transfer(0x80 | (1 << bufno));
	digitalWrite(m_CSpin, HIGH);
	
	uint8_t st = status();
	
	int i(0);
	for(;i<20 && (st & (4 << (2*bufno)));i++) // Wait for message being sent
	{
		delay(10);
		st = status();
	}
	
	if(i==20) // Abord
	{
		bitModify(0x30+0x10*bufno, 0x80, 0x00);
	}
	
	return i < 20; // if we reach 20, the message is not sent.
}

void MCP2515::setCanSpeed(CAN_speed s)
{
	uint8_t buf[3];
	buf[0] = 0x01; // CNF3
	buf[1] = 0xD1; // CNF2
	buf[2] = (uint8_t)s & 0x3F; // CNF1
	writeRegister(0x28, buf, 3);
}

uint8_t MCP2515::status()
{
	digitalWrite(m_CSpin, LOW);
	SPI.transfer(0xA0);
	uint8_t ret = SPI.transfer(0x00);
	digitalWrite(m_CSpin, HIGH);
	return ret;
}

uint8_t MCP2515::rxStatus()
{
	digitalWrite(m_CSpin, LOW);
	SPI.transfer(0xB0);
	uint8_t ret = SPI.transfer(0x00);
	digitalWrite(m_CSpin, HIGH);
	return ret;
}

uint8_t MCP2515::canStatus()
{
	uint8_t val;
	readRegister(0x0E, &val, 1);
	return val;
}


void MCP2515::setMode(ModeOfOperation mode)
{
	bitModify(0x0F,0xE0,(uint8_t)mode);
	//writeRegister();
	uint8_t val(0xff);
	
	while(val != mode)
	{
		val = canStatus()&0xE0;
	} // Wait for the right mode
}

uint8_t MCP2515::getMessageControlRegister(MessageBuffer mb)
{
	uint8_t val;
	readRegister((uint8_t)mb,&val,1);
	return val;
}

void MCP2515::setMessageControlRegister(MessageBuffer mb, uint8_t conf)
{
	writeRegister((uint8_t)mb, &conf, 1);
}

void MCP2515::setupAcceptanceFilter(AcceptanceFilter af, uint8_t data[4])
{
	writeRegister((uint8_t)af, data, 4);
}

void MCP2515::setOneShot(bool en)
{
	bitModify(0x0F,0x08, ((en)?0x08:0x00));
}

void MCP2515::setAbordFlag(bool en)
{
	bitModify(0x0F,0x10, ((en)?0x10:0x00));
}

void MCP2515::CANConfig(uint8_t data[3])
{
	writeRegister(0x28, data,3);
}

void MCP2515::clearInterrupts()
{
	uint8_t val(0);
	writeRegister(0x2C, &val, 1);
}

uint8_t MCP2515::getInterrupts()
{
	uint8_t val(0);
	readRegister(0x2C, &val, 1);
	return val;
}

void MCP2515::setupInterrupts(uint8_t f)
{
	writeRegister(0x2B, &f, 1);
}

uint8_t MCP2515::errorStatus()
{
	uint8_t val;
	readRegister(0x2D, &val, 1);
	return val;
}

void MCP2515::readRegister(uint8_t address, uint8_t* buf, uint8_t nRead)
{
	digitalWrite(m_CSpin, LOW);
	SPI.transfer(0x03);
	SPI.transfer(address);
	for(uint8_t i(0);i<nRead;i++) buf[i] = SPI.transfer(0x00);
	digitalWrite(m_CSpin, HIGH);
}

void MCP2515::writeRegister(uint8_t address, uint8_t* buf, uint8_t nWrite)
{
	digitalWrite(m_CSpin, LOW);
	SPI.transfer(0x02);
	SPI.transfer(address);
	for(uint8_t i(0);i<nWrite;i++) SPI.transfer(buf[i]);
	digitalWrite(m_CSpin, HIGH);
}

void MCP2515::bitModify(uint8_t address, uint8_t mask, uint8_t data)
{
	digitalWrite(m_CSpin, LOW);
	SPI.transfer(0x05);
	SPI.transfer(address);
	SPI.transfer(mask);
	SPI.transfer(data);
	digitalWrite(m_CSpin, HIGH);
}
