#include "CAN4Home.h"

void watchdogConfig(uint8_t x) {
#ifdef WDCE //does it have a Watchdog Change Enable?
 #ifdef WDTCSR
  WDTCSR = _BV(WDCE) | _BV(WDE);
 #else
  WDTCR= _BV(WDCE) | _BV(WDE);
 #endif
#else //then it must be one of those newfangled ones that use CCP
  CCP=0xD8; //so write this magic number to CCP
#endif 

#ifdef WDTCSR
  WDTCSR = x;
#else
  WDTCR= x;
#endif
}

CAN4Home::CAN4Home(uint8_t address, uint8_t group, uint8_t CS, bool snif) :
	m_nodeAddress(address),
	m_groupAddress(group),
	m_controller(CS),
	m_chans(nullptr),
	m_chanCount(0),
	m_sniffer(snif)
{
	addChannel(UINT8, 1, 0); // Chan0 is reserved for number of channels
	//*
	
	m_controller.reset();
	delay(100);
	m_controller.setMode(MCP2515::MODE_CONFIG);
	m_controller.setCanSpeed(MCP2515::speed_125kbps);
	
	uint8_t mask[4];
	mask[0] = 0xFF;
	mask[1] = 0xE3;
	mask[2] = 0x00;
	mask[3] = 0x00;
	
	uint8_t filter[4];
	filter[0] = m_nodeAddress;
	filter[1] = ((m_groupAddress & 0x1C) << 3) | (m_groupAddress & 0x03) | 0x08;
	filter[2] = 0x00;
	filter[3] = 0x00;
	
	// Receive only packet addressed to node or broadcast
	m_controller.setupAcceptanceFilter(MCP2515::RXM0, mask);
	m_controller.setupAcceptanceFilter(MCP2515::RXF0, filter);
	filter[0] = 0xFF;
	m_controller.setupAcceptanceFilter(MCP2515::RXF1, filter);
	m_controller.setMessageControlRegister(MCP2515::RX0, ((m_sniffer) ? 0x64 : 0x04)); // Enable filter if not sniffer
	
	m_controller.setupInterrupts(MCP2515::INT_RX0 | MCP2515::INT_RX1);
	
	m_controller.setMode(MCP2515::MODE_NORMAL);//*/
}

bool CAN4Home::sendPacket(const CANPacket& p)
{
	uint8_t buf[13];
	
	buf[0] = p.nodeId;
	buf[1] = ((p.groupId & 0x1C) << 3) | (p.groupId & 0x3) | 0x08;
	buf[2] = p.command;
	buf[3] = p.cmdopt;
	buf[4] = p.N;
	if(p.isRequest) buf[4] |= 0x40;
	for(int i(0);i<p.N;i++) buf[i+5] = p.data[i];
	
	return m_controller.send(0, buf, p.N+5);
}

CAN4Home::CANPacket const& CAN4Home::readNextPacket()
{
	uint8_t buf[13];
	bool request;
	// Read packet, set cid, buf and bufc
	
	//for(uint8_t i(0);i<13;i++) buf[i] = 0;
	
	uint8_t status = m_controller.status();
	uint8_t bufno;
	
	m_lastPacket.N = 0;
	m_lastPacket.nodeId = 0;
	m_lastPacket.command = 0;
 	m_lastPacket.cmdopt = 0;
	m_lastPacket.isRequest = false;
	m_lastPacket.groupId = 0;
	
	if(status & 0x01) bufno = 0;
	else if(status & 0x02) bufno = 1;
	else return m_lastPacket;
	
	m_controller.readRxBuffer(bufno,buf,13);
	
	m_lastPacket.N = buf[4] & 0x0F;
	m_lastPacket.nodeId = buf[0];
	m_lastPacket.command = buf[2];
 	m_lastPacket.cmdopt = buf[3];
	m_lastPacket.isRequest = buf[4] & 0x40;
	m_lastPacket.groupId = ((buf[1]&0xE0) >> 3) | (buf[1] & 0x03);
	
	for(int i(0);i<m_lastPacket.N;i++) {
		m_lastPacket.data[i] = buf[i+5];
	}
	
	if(m_sniffer) return m_lastPacket;

	Channel *c = m_chans;
	
	if(m_lastPacket.command < CMD_DEBUG)
	{
		if(m_lastPacket.cmdopt < m_chanCount)
		{
			for(int i(0);i<m_lastPacket.cmdopt;i++) c = c->next;
		}
		else
		{
		// Emit error chan out of range packet
			return m_lastPacket;
		}
	}
	
	if(m_lastPacket.command == CMD_DATA)
	{
		if(request && c->onRequest)
		{
			c->onRequest(m_lastPacket.data, m_lastPacket.N, m_lastPacket.cmdopt);
			writeData(m_lastPacket.cmdopt, m_lastPacket.data, m_lastPacket.N, false);
		}
		
		if(m_lastPacket.isRequest && m_lastPacket.cmdopt == 0) // number of channels for chan0 (onRequest is null)
		{
			buf[0] = m_chanCount;
			writeData(m_lastPacket.cmdopt, buf, 1, false);
		}
	}
	
	if(m_lastPacket.command == CMD_WRITEDATA)
	{
		if(!request && c->onWrite)
		{
			c->onWrite(m_lastPacket.data, m_lastPacket.N, m_lastPacket.cmdopt);
		}
	}
	
	if(m_lastPacket.command == CMD_CHANEVENT)
	{
		c->event = true;
	}
	
	if(m_lastPacket.command == CMD_CHANDESC)
	{
		sendChanDesc(m_lastPacket.cmdopt, c);
	}
	
	if(m_lastPacket.command == CMD_REBOOT)
	{
		watchdogConfig(WATCHDOG_16MS);
		for(;;) ;
	}
	
	if(m_lastPacket.command == CMD_SERIAL && m_lastPacket.isRequest)
    {
        // Send serial number
        sendSerial();
    }
    
    if(m_lastPacket.command == CMD_DEVSIGN && m_lastPacket.isRequest)
    {
        // Send serial number
        sendSignature();
    }
	//*/
	
	return m_lastPacket;
}

void CAN4Home::sendSerial()
{
    uint8_t buf[13];
	buf[0] = m_nodeAddress;
	buf[1] = ((m_groupAddress & 0x1C) << 3) | (m_groupAddress & 0x3) | 0x08;
	buf[2] = CMD_SERIAL;
	buf[4] = 0x08;
	
    uint8_t sid(0x0E);
    buf[3] = boot_signature_byte_get(sid++);
    
    for(uint8_t i(5); i<13; i++) {
        buf[i] = boot_signature_byte_get(sid++);
        if(sid == 0x14) sid++;
    }
	
	m_controller.send(0, buf, 13);
}

void CAN4Home::sendSignature()
{
    uint8_t buf[8];
	buf[0] = m_nodeAddress;
	buf[1] = ((m_groupAddress & 0x1C) << 3) | (m_groupAddress & 0x3) | 0x08;
	buf[2] = CMD_DEVSIGN;
    buf[3] = 0;
	buf[4] = 0x03;
    buf[5] = boot_signature_byte_get(0x0000);
    buf[6] = boot_signature_byte_get(0x0002);
    buf[7] = boot_signature_byte_get(0x0004);
	
	m_controller.send(0, buf, 8);
}


void CAN4Home::sendChanDesc(uint8_t chid, Channel *c)
{
	uint8_t buf[6];
	buf[0] = m_nodeAddress;
	buf[1] = ((m_groupAddress & 0x1C) << 3) | (m_groupAddress & 0x3) | 0x08;
	buf[2] = CMD_CHANDESC;
	buf[3] = chid;
	buf[4] = 1;
	uint8_t tt = c->type;
	tt |= (c->onRequest == 0) ? 0x00 : 0x80;
	tt |= (c->onWrite == 0) ? 0x00 : 0x40;
	buf[5] = tt;
	
	m_controller.send(0, buf, 6);
}

void CAN4Home::writeData(uint8_t chanid, uint8_t* data, uint8_t count, bool event, bool onlyOnce)
{
	if(chanid >= m_chanCount) return;
	
	if(event)
	{
		Channel *c = m_chans;
		for(int i(0);i<chanid;i++) c = c->next;
		
		if(!c->event) return;
	}
	
	// Message construction
	uint8_t buf[13];
	
	for(uint8_t i(0);i<13;i++) buf[i] = 0;
	
	buf[0] = m_nodeAddress;
	buf[1] = ((m_groupAddress & 0x1C) << 3) | (m_groupAddress & 0x3) | 0x08;
	buf[2] = CMD_DATA;
	buf[3] = chanid;
	count = buf[4] = (count > 8) ? 8 : count;
	
	for(uint8_t i(0);i<count;i++)
	{
		buf[i+5] = data[i];
	}
	
	m_controller.send(0,buf,5+count);
}

uint8_t CAN4Home::addChannel(ChanType type, uint8_t N, void (*onRequest)(uint8_t *, uint8_t, uint8_t), void (*onWrite)(uint8_t *, uint8_t, uint8_t))
{
	Channel *c = new Channel;
	c->next = nullptr;
	c->onRequest = onRequest;
	c->onWrite = onWrite;
	c->type = ((uint8_t)type)&0x3F; // Prevent READ and WRITE to be set without function pointer
	c->numBytes=N;
	c->event = true;
	
	if(onRequest) c->type |= (uint8_t)READ;
	if(onWrite) c->type |= (uint8_t)WRITE;
	
	if(N > 8) goto fail;
	
	switch(type)
	{
		case UINT16:
		case INT16:
			if(!(N%2 == 0)) goto fail;
			break;
		case UINT32:
		case INT32:
		case FLOAT32:
			if(!(N%4 == 0)) goto fail;
			break;
		case UINT64:
		case INT64:
		case FLOAT64:
			if(N!=8) goto fail;
			break;
		case SWITCH:
		case CONTACT:
		case RELHUM:
			c->numBytes = 1;
			break;
		case DIMMER:
		case TEMP:
		case ROLLER:
			c->numBytes = 2;
			break;
		case COLOR:
			c->numBytes = 4;
		default:
			break;
	}
	
	return addChannel(c);
fail:
	delete c;
	return 0;
}

uint8_t CAN4Home::addChannel(Channel* chan)
{
	Channel *c = m_chans;
	
	if(!c)
	{
		m_chans = chan;
		return m_chanCount++;
	}
	
	while(c->next) c = c->next; // Go to last element in chained list
	c->next = chan;
	return m_chanCount++;
}

