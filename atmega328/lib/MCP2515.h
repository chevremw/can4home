

#include <SPI.h>

class MCP2515
{
	
public:
	
	typedef enum {
		MODE_CONFIG = 0x80,
		MODE_NORMAL = 0x00,
		MODE_SLEEP = 0x20,
		MODE_LISTENONLY = 0x60,
		MODE_LOOPBACK = 0x40
	} ModeOfOperation;
	
	typedef enum {
		TX0 = 0x30,
		TX1 = 0x40,
		TX2 = 0x50,
		RX0 = 0x60,
		RX1 = 0x70
	} MessageBuffer;
	
	typedef enum {
		RXM0 = 0x20,
		RXM1 = 0x24,
		RXF0 = 0x00,
		RXF1 = 0x04,
		RXF2 = 0x08,
		RXF3 = 0x10,
		RXF4 = 0x14,
		RXF5 = 0x18		
	} AcceptanceFilter;
	
	typedef enum {
		INT_MERROR = 0x80,
		INT_WAKEUP = 0x40,
		INT_ERROR = 0x20,
		INT_TX2 = 0x10,
		INT_TX1 = 0x08,
		INT_TX0 = 0x04,
		INT_RX1 = 0x02,
		INT_RX0 = 0x01
	} InterruptFlags;
	
	typedef enum {
		speed_500kbps 	= 0x00,
		speed_250kbps 	= 0x01,
		speed_125kbps 	= 0x03,
		speed_62p5kbps 	= 0x07
	} CAN_speed;
	
	MCP2515(uint8_t CSpin);
	void reset();
	void setCanSpeed(CAN_speed);
	bool readRxBuffer(uint8_t bufno, uint8_t* buf, uint8_t nRead);
	bool send(uint8_t bufno, uint8_t* buf, uint8_t nWrite);
	uint8_t status();
	uint8_t rxStatus();
	uint8_t canStatus();
	void setMode(ModeOfOperation);
	void setOneShot(bool);
	void setAbordFlag(bool);
	uint8_t getMessageControlRegister(MessageBuffer);
	void setMessageControlRegister(MessageBuffer, uint8_t conf);
	void setupAcceptanceFilter(AcceptanceFilter af, uint8_t[4]);
	void CANConfig(uint8_t[3]);
	void setupInterrupts(uint8_t f);
	void clearInterrupts();
	uint8_t getInterrupts();
	uint8_t errorStatus();
	
private:
	void readRegister(uint8_t address, uint8_t* buf, uint8_t nRead);
	void writeRegister(uint8_t address, uint8_t* buf, uint8_t nWrite);
	void bitModify(uint8_t address, uint8_t mask, uint8_t data);
	
	uint8_t m_CSpin;
};
