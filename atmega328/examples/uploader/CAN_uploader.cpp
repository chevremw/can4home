#include "CAN_uploader.h"

// CAN event available
ISR(INT0_vect)
{
	m_readyRead = true;
}

void setup()
{	
	// USART setting
	#ifndef SINGLESPEED
	UART_SRA = _BV(U2X0); //Double speed mode USART0
	#endif
	UART_SRB = _BV(RXEN0) | _BV(TXEN0);
	UART_SRC = _BV(UCSZ00) | _BV(UCSZ01);
	UART_SRL = (uint8_t)BAUD_SETTING;
	
	EICRA = 0x02;    // Trigger on falling edge
	EIMSK = 0x01; 
	
	m_readyRead = false;
	m_waitCommand = false;
	
	m_protocol = new CAN4Home(0x01, 0x00, 10, true);
}

void loop()
{
	cli();
	if(m_readyRead) // Treat CAN event
	{
		m_readyRead = false;
		sei();
		CAN4Home::CANPacket pkt = m_protocol->readNextPacket();
		
		if(pkt.command == BLD_PARAMS) {
			if(m_next == STK_READ_SIGN) {
				putch(pkt.data[2]);
				putch(pkt.data[3]);
				putch(pkt.data[4]);
			}
			else if(m_next == STK_SW_MAJOR)
			{
				putch(pkt.data[0]);
			}
			else if(m_next == STK_SW_MINOR)
			{
				putch(pkt.data[1]);
			}
			putch(STK_OK);
			
			m_waitCommand = false;
            m_next = 0x00;
		}
		else if(pkt.command == BLD_READFLASH)
        {
			//putch(pkt.N);
            for(int i(0);i<pkt.N;i++) putch(pkt.data[i]);
			
            if(m_length > 8)
            {
                m_length -= 8;
                address.word += 8;
            
                pkt.N = 2;
                pkt.data[0] = address.bytes[0];
                pkt.data[1] = address.bytes[1];
                
				//delay(10);
                m_protocol->sendPacket(pkt);
            }
            else
            {// termination
                m_waitCommand = false;
                putch(STK_OK);
            }
        }
        else if(pkt.command == BLD_WRITEFLASH)
		{
			if(m_next == STK_PROG_PAGE)
			{
				verifySpace();
				putch(STK_OK);
				m_next = 0x00;
				m_waitCommand = false;
			}
		}
        else if(pkt.command == CMD_NODEBLD)
        {
            if(m_next == SET_DEVICE_ADDRESS)
            {
                pkt.command = CMD_ENTERBLD;
                pkt.cmdopt = 0x00;
                pkt.N = 0;
                pkt.isRequest = true;
                
                m_protocol->sendPacket(pkt);
                m_next = 0x00;
				m_waitCommand = false;
            }
        }
	}
	
	sei();
	
	if(!m_waitCommand && serialAvailable())
	{
		uint8_t ch = getch();
		
		CAN4Home::CANPacket pkt;
		pkt.nodeId = (uint8_t) 0xBB;
		pkt.groupId = (uint8_t) 0x1F;
		
		if(ch == STK_GET_PARAMETER || ch == STK_READ_SIGN) {
			
			pkt.command = BLD_PARAMS;
			pkt.cmdopt = 0;
			pkt.N = 5;
			pkt.isRequest = true;
			
			m_protocol->sendPacket(pkt);
			
			if(ch == STK_GET_PARAMETER) m_next = getch();
			else m_next = ch; 
			verifySpace();
			/*
			* Send optiboot version as "SW version"
			* Note that the references to memory are optimized away.
			*/
			if (m_next == STK_SW_MINOR || m_next == STK_SW_MAJOR || m_next == STK_READ_SIGN) {
				m_waitCommand = true;
			} else {
				/*
				* GET PARAMETER returns a generic 0x03 reply for
				* other parameters - enough to keep Avrdude happy
				*/
				putch(0x03);
				putch(STK_OK);
			}
		}
		else if(ch == STK_SET_DEVICE) {
			// SET DEVICE is ignored
			getNch(20);
            putch(STK_OK);
		}
		else if(ch == STK_SET_DEVICE_EXT) {
			// SET DEVICE EXT is ignored
			getNch(4);
            putch(STK_OK);
		}
		else if(ch == STK_LOAD_ADDRESS) {
			// LOAD ADDRESS
			address.bytes[0] = getch();
			address.bytes[1] = getch();

			address.word *= 2; // Convert from word address to byte address
			verifySpace();
            putch(STK_OK);
		}
		else if(ch == STK_UNIVERSAL) {
			// UNIVERSAL command is ignored
			getNch(4);
			putch(0x00);
            putch(STK_OK);
		}
		/* Write memory, length is big endian and is in bytes */
		else if(ch == STK_PROG_PAGE) {
			// PROGRAM PAGE - we support flash programming only, not EEPROM
			uint8_t desttype;
			uint16_t length, savelength;
			length = getch()<<8;
			length |= getch();
			savelength = length;
			
			desttype = getch(); // 'F'flash or 'E'eeprom
			
			pkt.command = BLD_LOADFLASH;
			pkt.isRequest = false;

			// fill buffer
			uint8_t i(0);
			do {
				m_buffer[i++] = getch();
			} while (--length);
			
			// Send only if flash
			if(desttype == 'F') {
				while(length < savelength) {
					pkt.command = BLD_LOADFLASH;
					pkt.isRequest = false;
					pkt.cmdopt = length;
					for(i=0;length < savelength && i < 8;i++) {
						pkt.data[i] = m_buffer[length];
						length++;
					}
					pkt.N = i;
					
					if(!m_protocol->sendPacket(pkt))
						putch(STK_NOSYNC);
					
					delay(100);
				}
				
				// Send Write packet and wait for flash to be written
				pkt.command = BLD_WRITEFLASH;
				pkt.cmdopt = 0x00;
				pkt.isRequest = false;
				pkt.data[0] = address.bytes[0];
				pkt.data[1] = address.bytes[1];
				pkt.N = 2;
				
				m_protocol->sendPacket(pkt);			
				m_next = ch;
				m_waitCommand = true;
			}
		}
		/* Read memory block mode, length is big endian.  */
		else if(ch == STK_READ_PAGE) {
			uint8_t desttype;
			m_length = getch()<<8;
			m_length |= getch();

			desttype = getch();

			verifySpace();
			// send first 8bytes request. Send next when we receive it.
            
            pkt.command = BLD_READFLASH;
            pkt.cmdopt = 0x00;
            pkt.isRequest = false;
            pkt.data[0] = address.bytes[0];
            pkt.data[1] = address.bytes[1];
            pkt.N = 2;
            
            if(desttype == 'F')
                m_protocol->sendPacket(pkt);
            
            m_waitCommand = true;
		}
		else if(ch == SET_DEVICE_ADDRESS)
        {
            // Reboot desired node
            m_next = ch;
            
            pkt.nodeId = getch();
            pkt.groupId = getch();
            pkt.command = CMD_REBOOT;
            pkt.cmdopt = 0x00;
            pkt.N = 0;
            pkt.isRequest = false;
            
            m_protocol->sendPacket(pkt);
            m_waitCommand = true;
        }
		else {
			// This covers the response to commands like STK_ENTER_PROGMODE
			verifySpace();
            putch(STK_OK);
		}
	}
}

void verifySpace() {
  if(getch() == CRC_EOP) putch(STK_INSYNC);
  else putch(STK_NOSYNC);
}

uint8_t getch() {
	uint8_t ch;

	while(!(UART_SRA & _BV(RXC0)))  {  /* Spin */ }
	ch = UART_UDR;
	return ch;
}

void getNch(uint8_t N) {
	for(int i(0);i<N;i++) getch();
	verifySpace();
}

void putch(uint8_t ch) {
	while (!(UART_SRA & _BV(UDRE0))) {  /* Spin */ }
	UART_UDR = ch;
}

bool serialAvailable()
{
	return (UART_SRA & _BV(RXC0)) > 0;
}

