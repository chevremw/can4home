## Uploader

This example shows how to use an arduino connected on the CAN-bus to upload the application on other arduino. It implements the STK500v1 protocol, so, avrdude is used to upload the new sketch throw CAN-bus. The received should have the custom bootloader, or a compatible one.

A capacitor between RESET and GND is placed to prevent reset from the uploader.
