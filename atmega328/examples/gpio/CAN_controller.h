
#include <Arduino.h>
#include <CAN4Home.h>

volatile uint8_t m_currVal, m_chd;
volatile bool m_readyRead, m_timer;
volatile uint8_t id_switch(0), id_relay(0), id_temp(0);
volatile unsigned long m_counter;

#define N_SW 6
volatile uint32_t m_swCounter[N_SW];

CAN4Home *m_protocol; // Address, Group, CS

void setup();
void loop();

// Switches
void onContactRequest(uint8_t*, uint8_t, uint8_t);

// Digital Output
void onRelayRequest(uint8_t*, uint8_t, uint8_t);
void onRelayWritten(uint8_t*, uint8_t, uint8_t);

// Temperature
void onBoardTempRequest(uint8_t*, uint8_t, uint8_t);
