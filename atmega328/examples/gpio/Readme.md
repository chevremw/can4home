## GPIO example

This example shows how to use the library to interface general input output (contact and switches)

This implements 6 switch input (type CONTACT), 4 relay (type SWITCH), 1 temperature sensor.

NB: The type should be seen from server-side. 
