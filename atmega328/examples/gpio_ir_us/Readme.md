## GPIO with ultrasound and ir sensors example

This example shows how to use the library to interface general input output (contact and switches)

This implements 5 switch input (type CONTACT), 3 relay (type SWITCH), 1 temperature sensor, 1 ultrasonic sensor and 1 ir sensor.

NB: The type should be seen from server-side. 
