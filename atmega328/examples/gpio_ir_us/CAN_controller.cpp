#include "CAN_controller.h"


// Interrupts: detect CAN ready read and button press event
// TODO: configure Timer2 for every 1 min wakeup for analog reading
ISR(INT0_vect)
{
	m_readyRead = true;
}

ISR(TIMER2_OVF_vect)
{
	if(++m_counter > 3662) // Every minutes: trigger at 61.035Hz
	{
		m_counter = 0;
		m_timer = true;
	}
	
	for(int i(0);i<N_SW;i++)
	{
		m_swCounter[i]++;
	}
}

ISR(PCINT1_vect)
{
	uint8_t val = PINC xor 0x3F;
	m_chd = val xor m_currVal;
	m_currVal = val;
	
	for(int i(0);i<N_SW;i++)
	{
		uint8_t mask = 1 << i;
		if((m_chd & mask) && (val & mask))
		{
			m_swCounter[i] = 0;
		}
	}
}

void setup()
{
	DDRD |= 0xF0; // 4-7 as output
	PORTD &= 0x0F;
	
	DDRC = 0x00; // All PORTC as input (buttons)
	PORTC = 0x00; // No pullup
	
	// PC interrupts (inputs buttons)
	PCICR = 0x02;
	PCMSK1 = 0x1F;
	
	// Timer2 settings
	TIMSK2 = 0x01; // Timer2 overflow interrupt enabled
	TCCR2A = 0x00; // Normal operation
	TCCR2B = 0x07; // Normal op, prescaler /1024 => 16MHz/1024 = 15625 Hz. Interrupt on overflow (2^8=256): 15625/256 => 61.035Hz

	// Interrupt from CAN
	EICRA = 0x02;    // Trigger on falling edge
	EIMSK = 0x01; 
	
	// Power-save mode
	SMCR = 0x06;
	
	Serial.begin(115200);
	m_chd = 0;
	m_readyRead = false;
	Serial.println(F("Init"));
//*
	m_protocol = new CAN4Home(0x03, 0x00, 10, false);
	id_switch = m_protocol->addChannel(CAN4Home::CONTACT, 0, &onContactRequest);
	m_protocol->addChannel(CAN4Home::CONTACT, 0, &onContactRequest);
	m_protocol->addChannel(CAN4Home::CONTACT, 0, &onContactRequest);
	m_protocol->addChannel(CAN4Home::CONTACT, 0, &onContactRequest);
	m_protocol->addChannel(CAN4Home::CONTACT, 0, &onContactRequest);
	id_relay = m_protocol->addChannel(CAN4Home::SWITCH, 0, &onRelayRequest, &onRelayWritten);
	m_protocol->addChannel(CAN4Home::SWITCH, 0, &onRelayRequest, &onRelayWritten);
	m_protocol->addChannel(CAN4Home::SWITCH, 0, &onRelayRequest, &onRelayWritten);
	id_temp = m_protocol->addChannel(CAN4Home::TEMP, 0, &onBoardTempRequest);
    id_length = m_protocol->addChannel(CAN4Home::UINT16, 2, &onDistanceRequested);

//*/
}

void loop()
{
	bool didaction = false;
	// Switch change
	if(m_chd)
	{
		// Copy to local variable, with disabled interrupts
		cli();
		uint8_t chd = m_chd;
		uint8_t val = m_currVal;
		m_chd = 0;
		sei();
		
		for(int i(0);i<N_SW;i++)
		{
			uint8_t mask = 1 << i;
			//*
			if(chd & mask)
			{
				uint8_t T(0);
				onContactRequest(&T, 1, id_switch+i);
				//Serial.print(F("val to send: "));
				//Serial.println(T);
				m_protocol->writeData(id_switch+i, &T,1);
			}//*/
		}
		
		Serial.print(F("Changed: "));
		Serial.println(chd, BIN);
		Serial.print(F("VAL: "));
		Serial.println(val, BIN);
		didaction = true;
	}
	//*
	// Timer: every 1 minute
	if(m_timer)
	{
		uint8_t val[2];
		onBoardTempRequest(val, 2, id_temp);
		m_protocol->writeData(id_temp, val, 2);
		onDistanceRequested(val, 2, id_length);
		if(!(val[0] == 0 && val[1] == 0)) m_protocol->writeData(id_length, val, 2);
		cli();
		m_timer = false;
		sei();
		didaction = true;
	}
	
	// Read from CAN bus
	
	if(digitalRead(2) == LOW) // Treat CAN event
	{
		Serial.println(F("CAN ReadyRead"));
		Serial.flush();
		m_protocol->readNextPacket();
		m_readyRead = false;
		sei();
		didaction = true;
	}//*/
	
	Serial.flush();
	
	// Sleep waiting for interrupts, if no action where performed
	if(!didaction) {
		SMCR = SMCR | 0x01; // Enable sleep
		asm("sleep");
		SMCR = SMCR & 0xFE; // Disable sleep
		/*
		Serial.println(F("Waked up!!"));
		Serial.flush();//*/
	}
	else
	{
		Serial.println(F("Action performed: loop once again before going to sleep."));
		Serial.flush();
	}
}

void onBoardTempRequest(uint8_t*val, uint8_t, uint8_t)
{
	int tmp = analogRead(A6);
	float ratio = 1024./((float)tmp) -1.;
	float T = 298.15*4000./(298.15*log(ratio)+4000.);
	uint16_t toSend = T*100.;
	val[0] = (toSend & 0xFF00) >> 8;
	val[1] = toSend & 0x00FF;
}


void onContactRequest(uint8_t* val, uint8_t, uint8_t chid)
{
	if(chid < id_switch || chid >= id_relay) return;
	uint8_t idsh = chid - id_switch;
	uint8_t mask = 1 << idsh;
	*val = ((m_currVal & mask) ? 0x01 : ((m_swCounter[idsh] > 30) ? 0x03 : 0x02));
}

void onRelayRequest(uint8_t* val, uint8_t, uint8_t chid)
{
	if(chid < id_relay || chid >= id_temp) return;
	uint8_t tmp = PORTD & 0xF0;
	*val = (tmp & (1 << (chid - id_relay + 4))) ? 0x01 : 0x00;
}

void onRelayWritten(uint8_t* val, uint8_t, uint8_t chid)
{
	if(chid < id_relay || chid >= id_temp) return;
	if(!*val)
		PORTD &= 0xFF - (1 << (chid - id_relay + 4));
	else
		PORTD |= 1 << (chid - id_relay + 4);
}

void onDistanceRequested(uint8_t* val, uint8_t, uint8_t chid)
{
	// Pulse HIGH for 10us
	PORTD |= 0x10;
	delayMicroseconds(10);
	PORTD &= 0xEF;
	
	// Read pulse
	float duration = pulseIn(A5, HIGH);	
	uint16_t cm = duration / 2. * 0.038;

	val[0] = cm >> 8;
	val[1] = cm & 0x00FF;
}
