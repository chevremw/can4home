## OptiCANboot

This bootloader is a dual UART/CAN bootloader. It allows to update MCU code from either usb/serial converter as well as from CAN bus.

It fits into 1k bootloader section and is based on Optican project.

Tested:
* atmega328

Others are not tested.

### Licence

As Optiboot is licenced under GPLv2, OptiCANboot is also under GPLv2 licence.
