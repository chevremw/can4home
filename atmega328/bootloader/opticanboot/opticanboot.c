/* 
 * Copyright (C) 2019 William Chèvremont
 *
 * This file is part of OptiCANboot.
 *
 * OptiCANboot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * OptiCANboot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OptiCANboot. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Adapted from Optiboot:
 * (c) 2019 - The Optiboot project
 */


#define FUNC_READ 1
#define FUNC_WRITE 1

#define CMD_DATA		0x00
#define CMD_WRITEDATA	0x01
#define CMD_CHANDESC	0x02
#define CMD_CHANEVENT	0x03

#define CMD_DEBUG		0x70
#define CMD_INFO		0x71
#define CMD_WARNING		0x72
#define CMD_ERROR		0x73

#define CMD_REBOOT		0x80
#define CMD_NODEBLD		0x81
#define CMD_ENTERBLD	0x82
#define CMD_NODENORMAL	0x83

#define CMD_PINGPONG	0x90
#define CMD_SERIAL		0x91
#define CMD_FIRMVER		0x92
#define CMD_DEVSIGN		0x93
#define CMD_SETADDR		0x94

#define BLD_PARAMS		0xB0
#define BLD_LOADFLASH	0xB1
#define BLD_WRITEFLASH	0xB2
#define BLD_READFLASH	0xB3

// Naked version of opticanboot

#define OPTICANBOOT_MAJVER 1
#define OPTICANBOOT_MINVER 0

unsigned const int __attribute__((section(".version"))) 
optiboot_version = 256*(OPTICANBOOT_MAJVER) + OPTICANBOOT_MINVER;


#include <inttypes.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>

/*
 * optiboot uses several "address" variables that are sometimes byte pointers,
 * sometimes word pointers. sometimes 16bit quantities, and sometimes built
 * up from 8bit input characters.  avr-gcc is not great at optimizing the
 * assembly of larger words from bytes, but we can use the usual union to
 * do this manually.  Expanding it a little, we can also get rid of casts.
 */
typedef union {
	uint8_t  *bptr;
	uint16_t *wptr;
	uint16_t word;
	uint8_t bytes[2];
} addr16_t;

/*
 * Note that we use a replacement of "boot.h"
 * <avr/boot.h> uses sts instructions, but this version uses out instructions
 * This saves cycles and program memory, if possible.
 * boot_opt.h pulls in the standard boot.h for the odd target (?)
 */
#include "boot_opt.h"


// We don't use <avr/wdt.h> as those routines have interrupt overhead we don't need.

/*
 * pin_defs.h
 * This contains most of the rather ugly defines that implement our
 * ability to use UART=n and LED=D3, and some avr family bit name differences.
 */
#include "pin_defs.h"

/*
 * stk500.h contains the constant definitions for the stk500v1 comm protocol
 */
#include "stk500.h"

/* set the UART baud rate defaults */
#ifndef BAUD_RATE
#if F_CPU >= 8000000L
#define BAUD_RATE   57600L
#elif F_CPU >= 1000000L
#define BAUD_RATE   9600L   // 19200 also supported, but with significant error
#elif F_CPU >= 128000L
#define BAUD_RATE   4800L   // Good for 128kHz internal RC
#else // F_CPU
#define BAUD_RATE 1200L     // Good even at 32768Hz
#endif // F_CPU
#endif // BAUD_RATE

#ifdef SINGLESPEED
/* Single speed option */
#define BAUD_SETTING (( (F_CPU + BAUD_RATE * 8L) / ((BAUD_RATE * 16L))) - 1 )
#define BAUD_ACTUAL (F_CPU/(16 * ((BAUD_SETTING)+1)))
#else // SINGLESPEED
/* Normal U2X usage */
#define BAUD_SETTING (( (F_CPU + BAUD_RATE * 4L) / ((BAUD_RATE * 8L))) - 1 )
#define BAUD_ACTUAL (F_CPU/(8 * ((BAUD_SETTING)+1)))
#endif // SINGLESPEED

#if BAUD_ACTUAL <= BAUD_RATE
  #define BAUD_ERROR (( 100*(BAUD_RATE - BAUD_ACTUAL) ) / BAUD_RATE)
  #if BAUD_ERROR >= 5
    #error BAUD_RATE off by greater than -5%
  #elif BAUD_ERROR >= 2  && !defined(PRODUCTION)
    #warning BAUD_RATE off by greater than -2%
  #endif // BAUD_ERROR
#else // BAUD_ACTUAL
  #define BAUD_ERROR (( 100*(BAUD_ACTUAL - BAUD_RATE) ) / BAUD_RATE)
  #if BAUD_ERROR >= 5
    #error BAUD_RATE off by greater than 5%
  #elif BAUD_ERROR >= 2  && !defined(PRODUCTION)
    #warning BAUD_RATE off by greater than 2%
  #endif // BAUD_ERROR
#endif // BAUD_ACTUAL

#if BAUD_SETTING > 250
#error Unachievable baud rate (too slow) BAUD_RATE 
#endif // baud rate slow check
#if (BAUD_SETTING - 1) < 3
#if BAUD_ERROR != 0 // permit high bitrates (ie 1Mbps@16MHz) if error is zero
#error Unachievable baud rate (too fast) BAUD_RATE 
#endif
#endif // baud rate fast check

/* Watchdog settings */
#define WATCHDOG_OFF    (0)
#define WATCHDOG_16MS   (_BV(WDE))
#define WATCHDOG_32MS   (_BV(WDP0) | _BV(WDE))
#define WATCHDOG_64MS   (_BV(WDP1) | _BV(WDE))
#define WATCHDOG_125MS  (_BV(WDP1) | _BV(WDP0) | _BV(WDE))
#define WATCHDOG_250MS  (_BV(WDP2) | _BV(WDE))
#define WATCHDOG_500MS  (_BV(WDP2) | _BV(WDP0) | _BV(WDE))
#define WATCHDOG_1S     (_BV(WDP2) | _BV(WDP1) | _BV(WDE))
#define WATCHDOG_2S     (_BV(WDP2) | _BV(WDP1) | _BV(WDP0) | _BV(WDE))
#ifndef __AVR_ATmega8__
#define WATCHDOG_4S     (_BV(WDP3) | _BV(WDE))
#define WATCHDOG_8S     (_BV(WDP3) | _BV(WDP0) | _BV(WDE))
#endif


/*
 * We can never load flash with more than 1 page at a time, so we can save
 * some code space on parts with smaller pagesize by using a smaller int.
 */
#if SPM_PAGESIZE > 255
typedef uint16_t pagelen_t ;
#define GETLENGTH(len) len = getch()<<8; len |= getch()
#else // SPM_PAGESIZE
typedef uint8_t pagelen_t;
#define GETLENGTH(len) (void) getch() /* skip high byte */; len = getch()
#endif // SPM_PAGESIZE


/* Function Prototypes
 * The main() function is in init9, which removes the interrupt vector table
 * we don't need. It is also 'OS_main', which means the compiler does not
 * generate any entry or exit code itself (but unlike 'naked', it doesn't
 * supress some compile-time options we want.)
 */

void pre_main(void) __attribute__ ((naked)) __attribute__ ((section (".init8")));
int main(void) __attribute__ ((OS_main)) __attribute__ ((section (".init9"))) __attribute__((used));

#ifdef UART
void __attribute__((noinline)) __attribute__((leaf)) putch(char);
uint8_t __attribute__((noinline)) __attribute__((leaf)) getch(void) ;
void __attribute__((noinline)) verifySpace();
static inline void read_mem(uint8_t memtype,
			    addr16_t, pagelen_t len);
static void getNch(uint8_t);
#endif // UART

void __attribute__((noinline)) watchdogConfig(uint8_t x);
static inline void watchdogReset();
static inline void writebuffer(int8_t memtype, addr16_t mybuff,
			       addr16_t address, pagelen_t len);


#define bld_led_on()
#define bld_led_off()
#define can_led_on()
#define can_led_off()

#ifdef LEDS
#undef bld_led_on
#undef bld_led_off
#define bld_led_on() PORTB |= 0x01
#define bld_led_off() PORTB &= 0xFE
#endif // LEDS

#ifdef MCP2515
#ifdef LEDS
#undef can_led_on
#undef can_led_off
#define can_led_on() PORTB |= 0x02
#define can_led_off() PORTB &= 0xFD
#endif // LEDS
#define spisel() PORTB &= 0xFB
#define spiunsel() PORTB |= 0x04


// These functions uses _pkt as global buffer.
uint8_t inline __attribute__((leaf)) spitrans(uint8_t);
void __attribute__((noinline)) sendPacket();
void __attribute__((noinline)) readRx();
void __attribute__((noinline)) spitransm(uint8_t);
uint8_t  __attribute__((noinline)) canst();
#endif // MCP2515

/*
 * RAMSTART should be self-explanatory.  It's bigger on parts with a
 * lot of peripheral registers.  Let 0x100 be the default
 * Note that RAMSTART (for optiboot) need not be exactly at the start of RAM.
 */
#if !defined(RAMSTART)  // newer versions of gcc avr-libc define RAMSTART
#define RAMSTART 0x100
#if defined (__AVR_ATmega644P__)
// correct for a bug in avr-libc
#undef SIGNATURE_2
#define SIGNATURE_2 0x0A
#elif defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
#undef RAMSTART
#define RAMSTART (0x200)
#endif // atmega type
#endif // RAMSTART

/* C zero initialises all global variables. However, that requires */
/* These definitions are NOT zero initialised, but that doesn't matter */
/* This allows us to drop the zero init code, saving us memory */

#ifdef MCP2515
static const addr16_t buff = {(uint8_t *)(RAMSTART)+30}; // Reserve 30 bytes for CAN operations (0..12 for packet 14..24 for uid)
static const addr16_t _pkt = {(uint8_t *)RAMSTART}; // Use addr16_t to allow _pkt to be const. (ans make the compiler cry if we try to change _pkt address.)
#else // MCP2515
static const addr16_t buff = {(uint8_t *)(RAMSTART)};
#endif // MCP2515

/* everything that needs to run VERY early */
void pre_main(void) {
  asm volatile (
    "	rjmp	1f\n" // Goto main function
    "   ret\n"
#ifdef MCP2515
#ifndef NO_INTERRUPT
//	"	rjmp	__vector_int_vect0\n" // INT0 interrupt vector //!\\ This is RESET+2WORD!!
	"	reti\n" // Only used for wakeup from sleep
#endif // NO_INTERRUPT
#endif // MCP2515
    "1:\n"
  );
}


/* main program starts here */
int main(void) {
  uint8_t ch;

  /*
   * Making these local and in registers prevents the need for initializing
   * them, and also saves space because code no longer stores to memory.
   * (initializing address keeps the compiler happy, but isn't really
   *  necessary, and uses 4 bytes of flash.)
   */
  register addr16_t address;
  register pagelen_t length;

  // After the zero init loop, this is the first code to run.
  //
  // This code makes the following assumptions:
  //  No interrupts will execute
  //  SP points to RAMEND
  //  r1 contains zero
  //
  // If not, uncomment the following instructions:
  // cli();
  asm volatile ("clr __zero_reg__");

#if defined(__AVR_ATmega8__) || defined(__AVR_ATmega8515__) ||		\
    defined(__AVR_ATmega8535__) || defined (__AVR_ATmega16__) || 	\
    defined (__AVR_ATmega32__) || defined (__AVR_ATmega64__)  ||	\
    defined (__AVR_ATmega128__) || defined (__AVR_ATmega162__)
  SP=RAMEND;  // This is done by hardware reset
#endif // Atmega type

  // Set up watchdog to trigger after 2s
  watchdogConfig(WATCHDOG_2S);
  
#ifdef MCP2515
  // Move interrupt table, enable INT0 on falling edge
#ifndef NO_INTERRUPT
  MCUCR = 0x01;
  MCUCR = 0x02;
  
  EICRA = 0x02;
  EIMSK = 0x01;
  asm volatile("sei"); // Enable interrupts
#endif // NO_INTERRUPT
  
  //*
  // First setup mcp2515 and wait for event

  // SCK(PB5), MOSI(PB3), SS(PB2), PB0 and PB1 as output, MISO(PB4) as input
  DDRB = 0x2F;
  SPCR = 0x51; // SPI enabled, MSTR and /16 (1MHz for 16MHz cristal)
  //SPSR = 0x00; //Done by reset, save 2 bytes
  
  bld_led_on();
  
  //* MCP2515 config: reduced to the strict minimum
  // Reset: auto go to CONFIG mode
  //_pkt.bptr[0] = 0xC0;
  //spitransm(1);
  
  /*
    7c24:	ee e0       	ldi	r30, 0x0E	; 14
    7c26:	f0 e0       	ldi	r31, 0x00	; 0
    7c28:	81 e2       	ldi	r24, 0x21	; 33
    7c2a:	80 93 57 00 	sts	0x0057, r24	; 0x800057 <__TEXT_REGION_LENGTH__+0x7e0057>
    7c2e:	84 91       	lpm	r24, Z
    7c30:	80 93 0d 01 	sts	0x010D, r24	; 0x80010d <_edata+0xd>
    7c34:	88 e0       	ldi	r24, 0x08	; 8
    7c36:	80 93 0e 01 	sts	0x010E, r24	; 0x80010e <_edata+0xe>
    7c3a:	af e0       	ldi	r26, 0x0F	; 15
    7c3c:	b1 e0       	ldi	r27, 0x01	; 1
    7c3e:	8f e0       	ldi	r24, 0x0F	; 15
    7c40:	91 e2       	ldi	r25, 0x21	; 33
    7c42:	e8 2f       	mov	r30, r24
    7c44:	f0 e0       	ldi	r31, 0x00	; 0
    7c46:	90 93 57 00 	sts	0x0057, r25	; 0x800057 <__TEXT_REGION_LENGTH__+0x7e0057>
    7c4a:	e4 91       	lpm	r30, Z
    7c4c:	ed 93       	st	X+, r30
    7c4e:	83 31       	cpi	r24, 0x13	; 19
    7c50:	09 f4       	brne	.+2      	; 0x7c54 <main+0x4e>
    7c52:	84 e1       	ldi	r24, 0x14	; 20
    7c54:	8f 5f       	subi	r24, 0xFF	; 255
    7c56:	88 31       	cpi	r24, 0x18	; 24
    7c58:	a1 f7       	brne	.-24     	; 0x7c42 <main+0x3c>
  //*/
  
  //                 0  1  2   3      4 5  6  7  8  9  A  B  C  D       E   F  10  11  12   13 14  15  16  17
  //                 0  1  2   3      4 5  6  7  8  9  10 11 12 13      14  15 16  17  18   19 20  21  22  23
  //* pkt should be [BB ED CMD CMDOPT N D0 D1 D2 D3 D4 D5 D6 D7 NODEBLD SN1 8  SN0 SN3 SN2 SN5 SN4 SN6 SN7 SN8]
  
  /*
  uint8_t displ = 0x0e;
  addr16_t uid;
  uid.bptr = _pkt.bptr + 13;
  *(uid.bptr++) = boot_signature_byte_get(displ++);
  *(uid.bptr++) = 0x08;
  do {
      *(uid.bptr++) = boot_signature_byte_get(displ);
      if(displ == 0x13) displ++; // skip 0x14
  } while(++displ != 0x18);//*/
  
  asm volatile(
        "ldi r30, 0x0E ; Z => sig addr\n"
        "ldi r31, 0x00\n"
        "ldi r28, 0x57 ; Y => lpm reg addr\n"
        "ldi r29, 0x00\n"
        "ldi r26, 0x0D ; X => SRAM addr\n"
        "ldi r27, 0x01\n"
        "ldi r24, 0x21 ; lmp operation\n"
        "ldi r25, %0 ; \n"
        "st X+, r25 ; Store nodebld\n"
        "st Y, r24 ; set lpm op\n"
        "lpm r25, Z+ ; load sn1 in tmp register (r25)\n"
        "st X+, r25 ; store to sram\n"
        "ldi r25, 0x08 ; \n"
        "st X+, r25 ; store the 8\n"
        "loopsn: st Y, r24 ; \n"
        "lpm r25, Z+ ; load sn n\n"
        "st X+, r25 ; store to ram\n"
        "cpi r30, 0x14 ; \n"
        "brne .+2 ; \n"
        "inc r30 ; Skip 0x14\n"
        "cpi r30, 0x18 ; \n"
        "brne loopsn \n"
        ::
        "i" (CMD_NODEBLD)
        :
        "r24", "r25", "r26", "r27", "r28", "r29", "r30", "r31"
);
  
  // Don't use spitransm for single byte transmission save 2 bytes.
  spisel();
  spitrans(0xC0);
  spiunsel();
  
  // Set to 125kbps with 8MHz cristal, enable reception interrupts
/*
  _pkt.bptr[0] = 0x02;
  _pkt.bptr[1] = 0x28;
  _pkt.bptr[2] = 0x01;
  _pkt.bptr[3] = 0xD1;
  _pkt.bptr[4] = 0x03;
  _pkt.bptr[5] = 0x03;
//*/
  uint8_t *t = _pkt.bptr;
  asm volatile(
      "ldi r24, 0x02\n"
      "st Z+, r24 ; Using st instead of sts save 2 bytes per instructions.\n"
      "ldi r24, 0x28\n"
      "st Z+, r24\n"
      "ldi r24, 0x01\n"
      "st Z+, r24\n"
      "ldi r24, 0xD1\n"
      "st Z+, r24\n"
      "ldi r24, 0x03\n"
      "st Z+, r24\n"
      "st Z+, r24\n"
      ::      "z" (t)
      : "r24");
  spitransm(6);
  
  // Acceptance mask+filter: reserved address by the protocol (filters activated by default, no BUKT)
  /*
  _pkt.bptr[1] = 0x20;
  _pkt.bptr[2] = 0xFF;
  _pkt.bptr[3] = 0xE3;//*/
  asm volatile(
      "ldi r24, 0x20\n"
      "st Z+, r24\n"
      "ldi r24, 0xFF\n"
      "st Z+, r24\n"
      "ldi r24, 0xE3\n"
      "st Z+, r24\n"
      ::      "z" (t+1)
      : "r24");
  spitransm(4);
  
  /*
  _pkt.bptr[1] = 0x00;
  _pkt.bptr[2] = 0xBB;
  _pkt.bptr[3] = 0xEB;
  //*/
    asm volatile(
      "ldi r24, 0x00\n"
      "st Z+, r24\n"
      "ldi r24, 0xBB\n"
      "st Z+, r24\n"
      "ldi r24, 0xEB\n"
      "st Z+, r24\n"
      ::      "z" (t+1)
      : "r24");
  spitransm(4);  
  
  // Mode Normal (use write register instead of bit modify: save some bits)
  /*
  _pkt.bptr[1] = 0x0F;
  _pkt.bptr[2] = 0x03;
  //*/
    asm volatile(
      "ldi r24, 0x0F\n"
      "st Z+, r24\n"
      "ldi r24, 0x03\n"
      "st Z+, r24\n"
      ::      "z" (t+1)
      : "r24");
  spitransm(3);
  
  //*/ // Send a "Node in bld mode" packet
  /*
  _pkt.bptr[0] = 0xBB;
  _pkt.bptr[1] = 0xEB;
  _pkt.bptr[2] = CMD_NODEBLD;
  _pkt.bptr[3] = _pkt.bptr[14];
  _pkt.bptr[4] = 0x08;
  for(uint8_t i=0;i<8;i++) _pkt.bptr[5+i] = _pkt.bptr[14+i];
  //*/
  asm volatile(
      "ldi r24, 0xBB\n"
      "st Z+, r24\n"
      "ldi r24, 0xEB\n"
      "st Z+, r24\n"
      "l1:ldd r24, Z+11\n"
      "st Z+, r24\n"
      "cpi r30, 0x0D\n"
      "brne l1\n"
      ::        
        "z" (t)
      : "r24");
  
  sendPacket();
  
//*/
  
  // Wait for a "Go to bootloader packet", before the check of reboot reason (reboot may be activated by wdt in software)
	/*
	for(uint32_t i=0;i<700000L;i++) { // Wait for CAN packet.  Can't be too long or the USART will fail.
		asm volatile("nop");
	}//*/
	{
        can_led_on();
		register uint16_t i asm("r24");
		uint8_t N = 30;
		do {
			i = 40000L;
			asm volatile("sbiw %0, 0x01\n"
					"brne .-4" : "=r" (i)); // 10ms delay
		} while(--N); // 300ms delay
        can_led_off();
	}
	
	if(canst() & 0x01) {
		readRx(); // Go to CAN mode on request. Else, check reset reason and may continue to USART mode.
		/*
        uint8_t i;
        for(i=2;i<14;i++) {
            if(*(_pkt.bptr+i) != *(_pkt.bptr+i+11))
                goto USART;
        }//*/
        /* 
    7d02:	e2 e0       	ldi	r30, 0x02	; 2
    7d04:	f1 e0       	ldi	r31, 0x01	; 1
    7d06:	91 91       	ld	r25, Z+
    7d08:	82 85       	ldd	r24, Z+10	; 0x0a
    7d0a:	98 13       	cpse	r25, r24
    7d0c:	5a c0       	rjmp	.+180    	; 0x7dc2 <usart>
    7d0e:	ee 30       	cpi	r30, 0x0E	; 14
    7d10:	81 e0       	ldi	r24, 0x01	; 1
    7d12:	f8 07       	cpc	r31, r24
    7d14:	c1 f7       	brne	.-16     	; 0x7d06 <l1+0x4a> //*/
    //*
        asm volatile(
            "ldi r30, 0x05 ; \n"
            "ldi r31, 0x01 ; \n"
            "chkpkt: ld r25, Z+ ; \n"
            "ldd r24, Z+10 ; \n"
            "cpse r25, r24 ; \n"
            "rjmp usart ; \n"
            "cpi r30, 0x0D ; \n"
            "brne chkpkt ; \n"
            );//*/
        goto CAN;
    
	}
#endif // MCP2515

//USART:
asm volatile("usart: ; \n");
    /*
   * Protect as much from MCUSR as possible for application
   * and still skip bootloader if not necessary
   * 
   * Code by MarkG55
   * see discusion in https://github.com/Optiboot/optiboot/issues/97
   */
    
// Save and clear reset reason
#if defined(__AVR_ATmega8515__) || defined(__AVR_ATmega8535__) ||	\
    defined(__AVR_ATmega16__)   || defined(__AVR_ATmega162__) ||	\
    defined (__AVR_ATmega128__)
  ch = MCUCSR;
  MCUCSR = 0;
#else // Atmega type
  ch = MCUSR;
  MCUSR = 0;
#endif // Atmega type
  
  if (ch != _BV(EXTRF)) { // Bootloader must be run if external request. Run application else.
	  /* 
	   * save the reset flags in the designated register
	   * This can be saved in a main program by putting code in .init0 (which
	   * executes before normal c init code) to save R2 to a global variable.
	   */
	  __asm__ __volatile__ ("mov r2, %0\n" :: "r" (ch));

	  // switch off watchdog
	  watchdogConfig(WATCHDOG_OFF);
		asm volatile ("clr __zero_reg__\n"
			"cli\n"
		);
		
		// Reset all stuffs
#ifndef NO_INTERRUPT
		MCUCR 	= 0x01;
		MCUCR 	= 0x00;
        
		EICRA 	= 0x00;
		EIMSK 	= 0x00;
#endif // NO_INTERRUPT

		SPCR 	= 0x00;
		DDRB 	= 0x00;
		PORTB	= 0x00;
		
	  // Note that appstart_vec is defined so that this works with either
	  // real or virtual boot partitions.
	   asm volatile (
	    // Jump to 'save' or RST vector

#ifdef RAMPZ
	    // use absolute jump for devices with lot of flash
	    "jmp 0\n"::
#else // RAMPZ
	    // use rjmp to go around end of flash to address 0
	    // it uses fact that optiboot_version constant is 2 bytes before end of flash
	    "rjmp optiboot_version+2\n"
	    //"jmp 0\n"
#endif //RAMPZ
	  );
  }


#ifdef UART
  #if defined(__AVR_ATmega8__) || defined (__AVR_ATmega8515__) ||	\
      defined (__AVR_ATmega8535__) || defined (__AVR_ATmega16__) ||	\
      defined (__AVR_ATmega32__)
  #ifndef SINGLESPEED
  UCSRA = _BV(U2X); //Double speed mode USART
  #endif //singlespeed
  UCSRB = _BV(RXEN) | _BV(TXEN);  // enable Rx & Tx
  UCSRC = _BV(URSEL) | _BV(UCSZ1) | _BV(UCSZ0);  // config USART; 8N1
  UBRRL = (uint8_t)BAUD_SETTING;
  #else // mega8/etc
    #ifdef LIN_UART
  //DDRB|=3;
  LINCR = (1 << LSWRES); 
  //LINBRRL = (((F_CPU * 10L / 32L / BAUD_RATE) + 5L) / 10L) - 1; 
  LINBRRL=(uint8_t)BAUD_SETTING;
  LINBTR = (1 << LDISR) | (8 << LBT0); 
  LINCR = _BV(LENA) | _BV(LCMD2) | _BV(LCMD1) | _BV(LCMD0); 
  LINDAT=0;
    #else
      #ifndef SINGLESPEED
  UART_SRA = _BV(U2X0); //Double speed mode USART0
      #endif
  UART_SRB = _BV(RXEN0) | _BV(TXEN0);
  UART_SRC = _BV(UCSZ00) | _BV(UCSZ01);
  UART_SRL = (uint8_t)BAUD_SETTING;
    #endif // LIN_UART
  #endif // mega8/etc

 /* Forever loop: exits by causing WDT reset */
  for (;;) {
      bld_led_off();
    /* get character from UART */
    ch = getch();
    
    bld_led_on();

    if(ch == STK_GET_PARAMETER) {
      unsigned char which = getch();
      verifySpace();
      /*
       * Send optiboot version as "SW version"
       * Note that the references to memory are optimized away.
       */
      if (which == STK_SW_MINOR) {
	  putch(optiboot_version & 0xFF);
      } else if (which == STK_SW_MAJOR) {
	  putch(optiboot_version >> 8);
      } else {
	/*
	 * GET PARAMETER returns a generic 0x03 reply for
         * other parameters - enough to keep Avrdude happy
	 */
	putch(0x03);
      }
    }
    else if(ch == STK_SET_DEVICE) {
      // SET DEVICE is ignored
      getNch(20);
    }
    else if(ch == STK_SET_DEVICE_EXT) {
      // SET DEVICE EXT is ignored
      getNch(4);
    }
    else if(ch == STK_LOAD_ADDRESS) {
      // LOAD ADDRESS
      address.bytes[0] = getch();
      address.bytes[1] = getch();
#ifdef RAMPZ
      // Transfer top bit to LSB in RAMPZ
      if (address.bytes[1] & 0x80) {
        RAMPZ |= 0x01;
      }
      else {
        RAMPZ &= 0xFE;
      }
#endif // RAMPZ
      address.word *= 2; // Convert from word address to byte address
      verifySpace();
    }
    else if(ch == STK_UNIVERSAL) {
#ifdef RAMPZ
      // LOAD_EXTENDED_ADDRESS is needed in STK_UNIVERSAL for addressing more than 128kB
      if ( AVR_OP_LOAD_EXT_ADDR == getch() ) {
        // get address
        getch();  // get '0'
        RAMPZ = (RAMPZ & 0x01) | ((getch() << 1) & 0xff);  // get address and put it in RAMPZ
        getNch(1); // get last '0'
        // response
        putch(0x00);
      }
      else {
        // everything else is ignored
        getNch(3);
        putch(0x00);
      }
#else // RAMPZ
      // UNIVERSAL command is ignored
      getNch(4);
      putch(0x00);
#endif // RAMPZ
    }
    /* Write memory, length is big endian and is in bytes */
    else if(ch == STK_PROG_PAGE) {
      // PROGRAM PAGE - we support flash programming only, not EEPROM
      uint8_t desttype;
      uint8_t *bufPtr;
      pagelen_t savelength;

      GETLENGTH(length);
      savelength = length;
      desttype = getch();

      // read a page worth of contents
      bufPtr = buff.bptr;
      do *bufPtr++ = getch();
      while (--length);

      // Read command terminator, start reply
      verifySpace();
      writebuffer(desttype, buff, address, savelength);
    }
    /* Read memory block mode, length is big endian.  */
    else if(ch == STK_READ_PAGE) {
      uint8_t desttype;
      GETLENGTH(length);

      desttype = getch();

      verifySpace();

      read_mem(desttype, address, length);
    }

    /* Get device signature bytes  */
    else if(ch == STK_READ_SIGN) {
      // READ SIGN - return what Avrdude wants to hear
      verifySpace();
      putch(SIGNATURE_0);
      putch(SIGNATURE_1);
      putch(SIGNATURE_2);
    }
    else if (ch == STK_LEAVE_PROGMODE) { /* 'Q' */
      // Adaboot no-wait mod
      watchdogConfig(WATCHDOG_16MS);
      verifySpace();
    }
    else {
      // This covers the response to commands like STK_ENTER_PROGMODE
      verifySpace();
    }
    putch(STK_OK);
  }
#endif // UART

#ifdef MCP2515

CAN:
	SMCR = 0x07; // Enable sleep, power-save mode
	for(;;) { // Forever loop, reset by WDT

#ifdef NO_INTERRUPT
    if(PIND & 0x04) continue;
    watchdogReset();
# else // NO_INTERRUPT
	watchdogReset();
    if(PIND & 0x04) asm volatile("sleep"); // sleep, waked up by CAN interrupt, or reset by watchdog
#endif // NO_INTERRUPT
		readRx();
		
		uint8_t N = _pkt.bptr[4] & 0x0F;
        addr16_t _d;
        _d.bptr = _pkt.bptr+5; // Convenient pointer to data start.
        uint8_t done = 0;
        can_led_on();
		
		if(_pkt.bptr[2] == BLD_PARAMS)
		{
            done = 1;
			_pkt.bptr[4] = 5;
			_pkt.bptr[5] = optiboot_version >> 8;
			_pkt.bptr[6] = optiboot_version & 0xFF;
			_pkt.bptr[7] = SIGNATURE_0;
			_pkt.bptr[8] = SIGNATURE_1;
			_pkt.bptr[9] = SIGNATURE_2;
		}//*/
		if(_pkt.bptr[2] == BLD_LOADFLASH) // LOAD
		{
			/* Load directly without buffer TODO: Refactor with do..while? (!DONE)
			for(uint8_t i=0;i<N;i+=2) {
				uint16_t dat = pkt[5+i] | (pkt[6+i] << 8);
				__boot_page_fill_short(pkt[3]+i,dat);
			}
			//*/
			done = 1;
			uint8_t p=_pkt.bptr[3];
			do {
				__boot_page_fill_short(p,*(_d.wptr++));
				p+=2;
			} while(N-=2);
		}
		else 
		{
			address.bytes[0] = _pkt.bptr[5];
			address.bytes[1] = _pkt.bptr[6];
			
			if(_pkt.bptr[2] == BLD_READFLASH)
			{
                done = 1;
				//*
				uint8_t N=8;
				do {
					// read a Flash byte and increment the address
					asm volatile ("lpm %0,Z+\n" : "=r" (ch), "=z" (address.bptr): "1" (address));
					*(_d.bptr++) = ch;
				} while(--N);
				_pkt.bptr[4] = 8; // Unset RTR bit
				//*/
			}
			else if(_pkt.bptr[2] == BLD_WRITEFLASH)
			{
                done = 1;
				__boot_page_erase_short(address.word);
				boot_spm_busy_wait();
				__boot_page_write_short(address.word);
				boot_spm_busy_wait();
	#if defined(RWWSRE)
				// Reenable read access to flash
				__boot_rww_enable_short();
	#endif // RWWSRZ
				_pkt.bptr[4] = 0x40;
			}
		}
		
		if(done)
            sendPacket(); // Resend (modified) packet to notify, if action was performed.
        can_led_off();
	}
#endif // MCP2515


}

#ifdef MCP2515
void spitransm(uint8_t N) {
    uint8_t* dat = _pkt.bptr;
	spisel();
	//for(uint8_t i=0;i<N;i++) spitrans(dat[i]);
	do spitrans(*(dat++)); while(--N);
	spiunsel();
}

uint8_t spitrans(uint8_t dat) {
	SPDR = dat;
	while(!(SPSR & (1 << SPIF)));
	return (SPDR);
}

void sendPacket() // No need of length: it is inside packet!
{
  uint8_t* dat=_pkt.bptr;
  spisel();
  spitrans(0x40);
  uint8_t N=13;
  do spitrans(*(dat++)); while(--N);
  spiunsel();
  
  // trigger RTS
  spisel();
  spitrans(0x81);
  spiunsel();
  
  // Wait for packet sended?
}

void readRx()
{
    uint8_t *dat = _pkt.bptr;
	spisel();
	spitrans(0x90);
	//for(uint8_t i=0;i<13;i++) dat[i] = spitrans(0x00);
	uint8_t N=13;
	do *(dat++) = spitrans(0x00); while(--N);
	spiunsel();
}

uint8_t canst() {
	spisel();
	spitrans(0xA0);
	uint8_t v = spitrans(0x00);
	spiunsel();
	return v;
}
#endif // MCP2515

#ifdef UART
void putch(char ch) {
  #ifndef LIN_UART
    while (!(UART_SRA & _BV(UDRE0))) {  /* Spin */ }
  #else // LIN_UART
    while (!(LINSIR & _BV(LTXOK)))   {  /* Spin */ }
  #endif // LIN_UART

  UART_UDR = ch;
}

uint8_t getch(void) {
  uint8_t ch;

#ifndef LIN_UART
  while(!(UART_SRA & _BV(RXC0)))  {  /* Spin */ }
  if (!(UART_SRA & _BV(FE0))) {
#else // LIN_UART
  while(!(LINSIR & _BV(LRXOK)))  {  /* Spin */ }
  if (!(LINSIR & _BV(LFERR))) {
#endif // LIN_UART
      /*
       * A Framing Error indicates (probably) that something is talking
       * to us at the wrong bit rate.  Assume that this is because it
       * expects to be talking to the application, and DON'T reset the
       * watchdog.  This should cause the bootloader to abort and run
       * the application "soon", if it keeps happening.  (Note that we
       * don't care that an invalid char is returned...)
       */
    watchdogReset();
  }

  ch = UART_UDR;
  return ch;
}

void getNch(uint8_t count) {
  do getch(); while (--count);
  verifySpace();
}

void verifySpace() {
  if (getch() != CRC_EOP) {
    watchdogConfig(WATCHDOG_16MS);    // shorten WD timeout
    for(;;)			      // and busy-loop so that WD causes
      ;				      //  a reset and app start.
  }
  putch(STK_INSYNC);
}
#endif // UART

// Watchdog functions. These are only safe with interrupts turned off.
void watchdogReset() {
  __asm__ __volatile__ (
    "wdr\n"
  );
}

void watchdogConfig(uint8_t x) {
#ifdef WDCE //does it have a Watchdog Change Enable?
 #ifdef WDTCSR
  WDTCSR = _BV(WDCE) | _BV(WDE);
 #else // WDTCSR
  WDTCR= _BV(WDCE) | _BV(WDE);
 #endif // WDTCSR
#else //WDCE ; then it must be one of those newfangled ones that use CCP
  CCP=0xD8; //so write this magic number to CCP
#endif // WDCE

#ifdef WDTCSR
  WDTCSR = x;
#else // WDTCSR
  WDTCR= x;
#endif // WDTCSR
}


/*
 * void writebuffer(memtype, buffer, address, length)
 */
void writebuffer(int8_t memtype, addr16_t mybuff,
			       addr16_t address, pagelen_t len)
{
    switch (memtype) {
    case 'E': // EEPROM

	for(;;)
	    ; // Error: wait for WDT
	break;
    default:  // FLASH
	/*
	 * Default to writing to Flash program memory.  By making this
	 * the default rather than checking for the correct code, we save
	 * space on chips that don't support any other memory types.
	 */
	{
	    // Copy buffer into programming buffer
	    uint16_t addrPtr = address.word;

	    /*
	     * Start the page erase and wait for it to finish.  There
	     * used to be code to do this while receiving the data over
	     * the serial link, but the performance improvement was slight,
	     * and we needed the space back.
	     */
#ifdef FOURPAGEERASE
	    if ((address.bytes[0] & ((SPM_PAGESIZE<<2)-1))==0) {
#endif // FOURPAGEERASE
	    __boot_page_erase_short(address.word);
	    boot_spm_busy_wait();
#ifdef FOURPAGEERASE
	    }
#endif // FOURPAGEERASE

	    /*
	     * Copy data from the buffer into the flash write buffer.
	     */
	    do {
		__boot_page_fill_short((uint16_t)(void*)addrPtr, *(mybuff.wptr++));
		addrPtr += 2;
	    } while (len -= 2);

	    /*
	     * Actually Write the buffer to flash (and wait for it to finish.)
	     */
	    __boot_page_write_short(address.word);
	    boot_spm_busy_wait();
#if defined(RWWSRE)
	    // Reenable read access to flash
	    __boot_rww_enable_short();
#endif // RWWSRE
	} // default block
	break;
    } // switch
}

#ifdef UART

static inline void read_mem(uint8_t memtype, addr16_t address, pagelen_t length)
{
    uint8_t ch;

	do {

#if defined(RAMPZ)
	    // Since RAMPZ should already be set, we need to use EPLM directly.
	    // Also, we can use the autoincrement version of lpm to update "address"
	    //      do putch(pgm_read_byte_near(address++));
	    //      while (--length);
	    // read a Flash and increment the address (may increment RAMPZ)
	    __asm__ ("elpm %0,Z+\n" : "=r" (ch), "=z" (address.bptr): "1" (address));
#else // RAMPZ
	    // read a Flash byte and increment the address
	    __asm__ ("lpm %0,Z+\n" : "=r" (ch), "=z" (address.bptr): "1" (address));
#endif // RAMPZ
	    putch(ch);
	} while (--length);
}
#endif // UART

