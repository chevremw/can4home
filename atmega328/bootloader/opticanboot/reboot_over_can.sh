#!/bin/bash
set -x

stty -F$ISPPORT $BAUDRATE ;

echo -e -n '\x0A\x'$NODEID'\x'$GRPID > $ISPPORT ;

avrdude -pm328p -P$ISPPORT -carduino -b$BAUDRATE -U flash:w:$1
